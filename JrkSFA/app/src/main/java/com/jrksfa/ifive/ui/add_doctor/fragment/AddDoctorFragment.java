package com.jrksfa.ifive.ui.add_doctor.fragment;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import com.jrksfa.ifive.MyApplication;
import com.jrksfa.ifive.R;
import com.jrksfa.ifive.adapter.spinner_adapter.AddDoctorAreaAdapter;
import com.jrksfa.ifive.datas.SessionManager;
import com.jrksfa.ifive.datas.models.requests.GeoLocation;
import com.jrksfa.ifive.datas.realm_models.AllProduct;
import com.jrksfa.ifive.datas.realm_models.AllTown;
import com.jrksfa.ifive.engine.JRKEngine;
import com.jrksfa.ifive.ui.add_doctor.AddDoctorActivity;
import com.jrksfa.ifive.ui.add_doctor.adapterDoctorAdd.FocusProductAdapter;
import com.jrksfa.ifive.ui.doctor_dcr.adapters.add.FocusProductDisplayListAdapter;
import com.jrksfa.ifive.ui.doctor_dcr.adapters.add.TownFromDisplayListAdapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.Realm;

public class AddDoctorFragment extends Fragment {

    View rootView;
    SessionManager sessionManager;
    Unbinder unbinder;
    Realm realm;
    DatePickerDialog.OnDateSetListener startDatePicker;
    DatePickerDialog.OnDateSetListener dobDatePicker;
    Calendar myCalendar;
    Calendar myCalendar1;
    AddDoctorActivity activity;
    @BindView(R.id.doctorSubmit)
    Button doctorSubmit;
    @BindView(R.id.add_from_area)
    Button addFromArea;
    @BindView(R.id.addDoctorName)
    EditText addDoctorName;
    @BindView(R.id.genMale)
    RadioButton genMale;
    @BindView(R.id.genFemale)
    RadioButton genFemale;
    @BindView(R.id.gender)
    RadioGroup gender;
    @BindView(R.id.addDoctorMobile)
    EditText addDoctorMobile;
    @BindView(R.id.addDoctorEmail)
    EditText addDoctorEmail;
    @BindView(R.id.married)
    RadioButton married;
    @BindView(R.id.unMarried)
    RadioButton unMarried;
    @BindView(R.id.maritalStatus)
    RadioGroup maritalStatus;
    @BindView(R.id.doa)
    TextView doa;
    @BindView(R.id.dob)
    TextView dob;
    @BindView(R.id.addDoctorAddress)
    EditText addDoctorAddress;
    @BindView(R.id.grade)
    Button grade;
    @BindView(R.id.dgree)
    Button dgree;
    @BindView(R.id.specialization)
    Button specialization;
    @BindView(R.id.state11)
    RadioButton state11;
    @BindView(R.id.specializationWise)
    RadioButton specializationWise;
    @BindView(R.id.states)
    RadioGroup states;
    @BindView(R.id.focusProducts)
    Button focusProducts;
    @BindView(R.id.foProd)
    LinearLayout foProd;
    @BindView(R.id.remarks)
    EditText remarks;
    @BindView(R.id.other)
    EditText other;
    @BindView(R.id.lat)
    TextView lat;
    @BindView(R.id.locPing)
    ImageView locPing;
    @BindView(R.id.lon)
    TextView lon;
    @BindView(R.id.locAddress)
    TextView locAddress;
    @BindView(R.id.fromTime)
    TextView fromTime;
    @BindView(R.id.toTime)
    TextView toTime;
    @BindView(R.id.docDays)
    Button docDays;
    @BindView(R.id.selectDoa)
    LinearLayout selcect_doa;
    @BindView(R.id.selectDob)
    LinearLayout selcect_dob;
    @BindView(R.id.selectFromTime)
    LinearLayout selcect_from_time;
    @BindView(R.id.selectToTime)
    LinearLayout selcect_to_time;
    LocationManager locationManager;
    AlertDialog.Builder chartDialog;
    AlertDialog chartAlertDialog;
    private int fromTownID;
    RecyclerView.LayoutManager mLayoutManager;
    List<AllProduct> focusProductID,otherProductID;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (AddDoctorActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.add_doctor, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        realm = Realm.getDefaultInstance();
        myCalendar = Calendar.getInstance();
        myCalendar1 = Calendar.getInstance();

        //doa.setText(JRKEngine.myInstance.getSimpleCalenderDate(myCalendar));
        startDatePicker = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                doa.setText(JRKEngine.myInstance.getSimpleCalenderDate(myCalendar));
            }
            };

        dobDatePicker = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar1.set(Calendar.YEAR, year);
                myCalendar1.set(Calendar.MONTH, monthOfYear);
                myCalendar1.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                dob.setText(JRKEngine.myInstance.getSimpleCalenderDate(myCalendar1));
            }
        };
        focusProductID = new ArrayList<>();
        return rootView;
    }

    @OnClick(R.id.add_from_area)
    public void fromArea(){
        View addItemView = LayoutInflater.from(activity)
                .inflate(R.layout.autosearch_recycler, null);
        chartDialog = new AlertDialog.Builder(activity);
        chartDialog.setView(addItemView);
        chartAlertDialog = chartDialog.show();
        chartAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RecyclerView townsDataList =  addItemView.findViewById(R.id.items_data_list);
        final EditText search_type =  addItemView.findViewById(R.id.search_type);
        TextView textTitle =  addItemView.findViewById(R.id.text_title);
        textTitle.setText("From Area");
        final AddDoctorAreaAdapter itemShowAdapter = new AddDoctorAreaAdapter(activity,
                realm.copyFromRealm(realm.where(AllTown.class).findAll()),
                this, fromTownID);
        townsDataList.setAdapter(itemShowAdapter);
        mLayoutManager = new LinearLayoutManager(activity);
        townsDataList.setLayoutManager(mLayoutManager);
        townsDataList.setItemAnimator(new DefaultItemAnimator());
        search_type.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = search_type.getText().toString().toLowerCase(Locale.getDefault());
                itemShowAdapter.filter(text);
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
    @OnClick(R.id.locPing)
    public void location(){
        getLatLonFromGPS();
    }

    @OnClick(R.id.selectDoa)
    public void selectDOA(){
        new DatePickerDialog(activity, startDatePicker, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    @OnClick(R.id.selectDob)
    public void selectDOB(){
        new DatePickerDialog(activity, dobDatePicker, myCalendar1.get(Calendar.YEAR),
                myCalendar1.get(Calendar.MONTH), myCalendar1.get(Calendar.DAY_OF_MONTH)).show();
    }

    @OnClick(R.id.selectFromTime)
    public void selectFromTime(){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                fromTime.setText( selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("From Time");
        mTimePicker.show();
    }

    @OnClick(R.id.selectToTime)
    public void selectToTime(){

        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                toTime.setText( selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("To Time");
        mTimePicker.show();

    }

    @OnClick(R.id.doctorSubmit)
    public void submit(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Please select your option");
        builder.setCancelable(true);
        builder.setNegativeButton("CANCEL", null);
        builder.setPositiveButton("SUBMIT NOW", null);
        builder.show();
    }

    @OnClick({R.id.state11, R.id.specializationWise})
    public void checkd(RadioButton radioButton){
        boolean checked = radioButton.isChecked();
        switch(radioButton.getId()){
            case R.id.state11:
                foProd.setVisibility(View.VISIBLE);
                break;
            case R.id.specializationWise:
                foProd.setVisibility(View.GONE);
                break;
        }
    }

    @OnClick(R.id.focusProducts)
    public void focusProduct(){
        View addItemView = LayoutInflater.from(activity)
                .inflate(R.layout.autosearch_multi_recycler, null);
        chartDialog = new AlertDialog.Builder(activity);
        chartDialog.setView(addItemView);
        chartAlertDialog = chartDialog.show();
        chartAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RecyclerView townsDataList =  addItemView.findViewById(R.id.items_data_list);
        final EditText search_type =  addItemView.findViewById(R.id.search_type);
        Button saveButton = addItemView.findViewById(R.id.save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chartAlertDialog.dismiss();
            }
        });
        TextView textTitle =  addItemView.findViewById(R.id.text_title);
        textTitle.setText("Focus Product");
        final FocusProductAdapter itemShowAdapter = new FocusProductAdapter(activity,
                realm.copyFromRealm(realm.where(AllProduct.class).findAll()),
                this, focusProductID);
        townsDataList.setAdapter(itemShowAdapter);
        mLayoutManager = new LinearLayoutManager(activity);
        townsDataList.setLayoutManager(mLayoutManager);
        townsDataList.setItemAnimator(new DefaultItemAnimator());
        search_type.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = search_type.getText().toString().toLowerCase(Locale.getDefault());
                itemShowAdapter.filter(text);
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });
    }

    @OnClick(R.id.dgree)
    public void alldegree(){/*
        View addItemView = LayoutInflater.from(activity)
                .inflate(R.layout.autosearch_multi_recycler, null);
        chartDialog = new AlertDialog.Builder(activity);
        chartDialog.setView(addItemView);
        chartAlertDialog = chartDialog.show();
        chartAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RecyclerView townsDataList =  addItemView.findViewById(R.id.items_data_list);
        final EditText search_type =  addItemView.findViewById(R.id.search_type);
        Button saveButton = addItemView.findViewById(R.id.save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chartAlertDialog.dismiss();
            }
        });
        TextView textTitle =  addItemView.findViewById(R.id.text_title);
        textTitle.setText("Focus Product");
        final FocusProductDisplayListAdapter itemShowAdapter = new FocusProductDisplayListAdapter(activity,
                realm.copyFromRealm(realm.where(AllProduct.class).findAll()),
                this, focusProductID);
        townsDataList.setAdapter(itemShowAdapter);
        mLayoutManager = new LinearLayoutManager(activity);
        townsDataList.setLayoutManager(mLayoutManager);
        townsDataList.setItemAnimator(new DefaultItemAnimator());
        search_type.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = search_type.getText().toString().toLowerCase(Locale.getDefault());
                itemShowAdapter.filter(text);
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });*/
    }


    public void getLatLonFromGPS() {
        Location lastKnownGPSLocation;
        Location lastKnownNetworkLocation;
        String gpsLocationProvider = LocationManager.GPS_PROVIDER;
        String networkLocationProvider = LocationManager.NETWORK_PROVIDER;
        try {
            locationManager = (LocationManager) MyApplication.getAppContext().getSystemService(Context.LOCATION_SERVICE);
            lastKnownGPSLocation = locationManager.getLastKnownLocation(gpsLocationProvider);
            lastKnownNetworkLocation = locationManager.getLastKnownLocation(networkLocationProvider);
            if (lastKnownGPSLocation != null) {
                lat.setText(lastKnownGPSLocation.getLatitude()+"");
                lon.setText(lastKnownGPSLocation.getLongitude()+"");
                locAddress.setText(JRKEngine.myInstance.getCompleteAddressString(getContext(),lastKnownGPSLocation.getLatitude(),lastKnownGPSLocation.getLongitude()));

            } else if (lastKnownNetworkLocation != null) {
                lat.setText(lastKnownNetworkLocation.getLatitude()+"");
                lon.setText(lastKnownNetworkLocation.getLongitude()+"");
                locAddress.setText(JRKEngine.myInstance.getCompleteAddressString(getContext(),lastKnownNetworkLocation.getLatitude(),lastKnownNetworkLocation.getLongitude()));

            } else {
                lat.setText("00");
                lon.setText("00");
                locAddress.setText("No Address");
            }
        } catch (SecurityException sec) {

        }
    }
    public void dismissAlert(){
        if(chartAlertDialog != null && chartAlertDialog.isShowing()){
            chartAlertDialog.dismiss();
        }
    }

    public void setSaleFocusProductPreference(AllProduct allProduct,boolean checked) {
        if(checked){
            if(!JRKEngine.myInstance.hasProductArrayObject(focusProductID,allProduct)){
                focusProductID.add(allProduct);
            }
        }else{
            for (AllProduct product:focusProductID){
                if (product.getProductId() == allProduct.getProductId()){
                    focusProductID.remove(product);
                }
            }
        }
        focusProducts.setText(JRKEngine.myInstance.getProductNamesArray(focusProductID));
    }

    public void setSaleFromTownPreference(int id, String name) {
        dismissAlert();
        fromTownID = id;
        addFromArea.setText(name);
    }
}
