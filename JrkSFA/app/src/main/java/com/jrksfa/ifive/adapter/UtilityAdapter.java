package com.jrksfa.ifive.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jrksfa.ifive.R;
import com.jrksfa.ifive.ui.add_doctor.AddDoctorActivity;
import com.jrksfa.ifive.datas.models.requests.UtilityModel;
import com.jrksfa.ifive.ui.chemist_location.ChemistLocation;
import com.jrksfa.ifive.ui.doctor_location.DoctorLocation;
import com.jrksfa.ifive.ui.reports.ReportsActivity;
import com.jrksfa.ifive.ui.stockist_location.StockistLocation;

public class UtilityAdapter extends RecyclerView.Adapter<UtilityAdapter.ViewHolder> {
    private UtilityModel[] itemsData;
    private static Context myContext;

    public UtilityAdapter(UtilityModel[] itemsData) {
        this.itemsData = itemsData;
    }


    @Override
    public UtilityAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {

        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.utility_list_item, null);


        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {


        viewHolder.txtViewTitle.setText(itemsData[position].getTitle());
        viewHolder.imgViewIcon.setImageResource(itemsData[position].getImageUrl());

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtViewTitle;
        public ImageView imgViewIcon;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            txtViewTitle = (TextView) itemLayoutView.findViewById(R.id.ut_it);
            imgViewIcon = (ImageView) itemLayoutView.findViewById(R.id.ut_ic);

            itemLayoutView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(v.getContext(), txtViewTitle.getText().toString()/* + Integer.toString(getAdapterPosition())*/, Toast.LENGTH_LONG).show();

                    String title = txtViewTitle.getText().toString();
                    if (title.equals("Reports")){
                        Intent n = new Intent(v.getContext(), ReportsActivity.class);
                        v.getContext().startActivity(n);
                    }else if (title.equals("Add Doctor")) {
                        Intent n = new Intent(v.getContext(), AddDoctorActivity.class);
                        v.getContext().startActivity(n);
                    }else if (title.equals("Update Doctor Location")){
                        Intent kk = new Intent(v.getContext(), DoctorLocation.class);
                        v.getContext().startActivity(kk);
                    }else if (title.equals("Update Chemist Location")){
                        Intent b = new Intent(v.getContext(), ChemistLocation.class);
                        v.getContext().startActivity(b);
                    }else if (title.equals("Update Stockist Location")){
                        Intent k = new Intent(v.getContext(), StockistLocation.class);
                        v.getContext().startActivity(k);

                    }
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return itemsData.length;
    }
}
