package com.jrksfa.ifive.ui.blank;

import android.os.Bundle;

import com.jrksfa.ifive.R;
import com.jrksfa.ifive.ui.base.BaseActivity;


public class BlankActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blank);
    }
}
