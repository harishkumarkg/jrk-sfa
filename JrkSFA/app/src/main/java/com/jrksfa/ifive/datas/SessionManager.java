package com.jrksfa.ifive.datas;

import android.content.Context;
import android.content.SharedPreferences;

import com.jrksfa.ifive.datas.models.responses.LoginResponse;
import com.jrksfa.ifive.engine.JRKEngine;

import java.util.Calendar;

public class SessionManager {

    public void setPreferences(Context context, LoginResponse loginResponse){
        SharedPreferences.Editor editor = context.getSharedPreferences("JRK", Context.MODE_PRIVATE).edit();
        editor.putString("Token", loginResponse.getToken());
        editor.putString("EmployeeId",loginResponse.getEmployeeId()+"");
        editor.putString("Username",loginResponse.getEmployeeName());
        editor.putString("Role",loginResponse.getDesignation()+"");
        editor.commit();
    }

    public String getToken(Context context){
        SharedPreferences prefs = context.getSharedPreferences("JRK", Context.MODE_PRIVATE);
        String token = prefs.getString("Token", null);
        return token;
    }

    public LoginResponse getUserData(Context context){
        SharedPreferences prefs = context.getSharedPreferences("JRK", Context.MODE_PRIVATE);
        LoginResponse loginResponse = new LoginResponse();
        loginResponse.setEmployeeId(JRKEngine.myInstance.convertStringToInt(prefs.getString("EmployeeId", null)));
        loginResponse.setDesignation(prefs.getString("Role", null));
        loginResponse.setEmployeeName(prefs.getString("Username", null));
        loginResponse.setToken(prefs.getString("Token", null));
        return loginResponse;
    }

    public void setSync(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences("JRK", Context.MODE_PRIVATE).edit();
        editor.putBoolean("sync", true);
        editor.putString("lastSync", JRKEngine.myInstance.getFullSimpleCalenderDate(Calendar.getInstance()));
        editor.commit();
    }

    public String getLastSync(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("JRK", Context.MODE_PRIVATE);
        return prefs.getString("lastSync", " - ");
    }

    public boolean getSync(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("JRK", Context.MODE_PRIVATE);
        return prefs.getBoolean("sync", false);
    }


    public void setStartEmployeeWorkTime(String calenderTime,Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences("JRK", Context.MODE_PRIVATE).edit();
        editor.putString("startTime", calenderTime);
        editor.commit();
    }

    public void setStartEmployeeNull(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences("JRK", Context.MODE_PRIVATE).edit();
        editor.putString("startTime", null);
        editor.commit();
    }

    public void setEndEmployeeWorkTime(String calenderTime,Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences("JRK", Context.MODE_PRIVATE).edit();
        editor.putString("endTime", calenderTime);
        editor.commit();
    }

    public void setEndEmployeeNull(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences("JRK", Context.MODE_PRIVATE).edit();
        editor.putString("endTime", null);
        editor.commit();
    }

    public void logoutSession(Context context){
        SharedPreferences sharedpreferences = context.getSharedPreferences("JRK", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.commit();
    }

    public String getStartTime(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("JRK", Context.MODE_PRIVATE);
        return prefs.getString("startTime", null);
    }

    public String getEndTime(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("JRK", Context.MODE_PRIVATE);
        return prefs.getString("endTime", null);
    }
}
