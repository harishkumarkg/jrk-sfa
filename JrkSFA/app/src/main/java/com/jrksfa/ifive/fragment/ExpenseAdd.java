package com.jrksfa.ifive.fragment;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;


import com.jrksfa.ifive.R;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ExpenseAdd extends Fragment {


    @BindView(R.id.below)
    Button below;
    @BindView(R.id.tpDate)
    TextView tpDate;
    @BindView(R.id.tpDateImage)
    ImageView tpDateImage;
    @BindView(R.id.indi)
    RadioButton indi;
    @BindView(R.id.doc)
    RadioButton doc;
    @BindView(R.id.expType)
    RadioGroup expType;
    @BindView(R.id.fromArea)
    TextView fromArea;
    @BindView(R.id.docExp)
    LinearLayout docExp;
    @BindView(R.id.indExp)
    LinearLayout indExp;
    @BindView(R.id.topPanel)
    LinearLayout topPanel;
    private int mYear, mMonth, mDay, mHour, mMinute;
    Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_expense_add, container, false);

        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @OnCheckedChanged({R.id.indi, R.id.doc})
             void expense(CompoundButton button, boolean checked) {

        if(checked) {
            switch (button.getId()) {
                case R.id.indi:
                    docExp.setVisibility(View.GONE);
                    indExp.setVisibility(View.VISIBLE);
                    break;
                case R.id.doc:
                    docExp.setVisibility(View.VISIBLE);
                    indExp.setVisibility(View.GONE);
                    break;
            }

    }
    }

    @OnClick(R.id.below)
    public void submit(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Please select your option");
        builder.setCancelable(true);
        builder.setNegativeButton("CANCEL", null);
        builder.setPositiveButton("SUBMIT NOW", null);
        builder.show();
    }

    @OnClick(R.id.tpDateImage)
    public void dateee() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        tpDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
