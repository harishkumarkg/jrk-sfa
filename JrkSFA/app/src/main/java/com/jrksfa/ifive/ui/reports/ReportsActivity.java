package com.jrksfa.ifive.ui.reports;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jrksfa.ifive.R;
import com.jrksfa.ifive.ui.base.BaseActivity;
import com.jrksfa.ifive.ui.doctor_location.DoctorLocation;
import com.jrksfa.ifive.ui.target.TargetActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReportsActivity extends BaseActivity {

    @BindView(R.id.ut_ic)
    ImageView utIc;
    @BindView(R.id.ut_it)
    TextView utIt;
    @BindView(R.id.right_arrow)
    ImageView rightArrow;
    @BindView(R.id.userTarget)
    LinearLayout userTarget;
    @BindView(R.id.doctorReport)
    LinearLayout doctorReport;
    @BindView(R.id.chemistReport)
    LinearLayout chemistReport;
    @BindView(R.id.distributerReport)
    LinearLayout distributerReport;
    @BindView(R.id.areaReport)
    LinearLayout areaReport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reports);
        ButterKnife.bind(this);
    }
    @OnClick(R.id.userTarget)
        public void target(){

        Intent s = new Intent(this, TargetActivity.class);
        startActivity(s);

    }

}
