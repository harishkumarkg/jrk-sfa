package com.jrksfa.ifive.ui.attendance;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.jrksfa.ifive.MyApplication;
import com.jrksfa.ifive.R;
import com.jrksfa.ifive.datas.SessionManager;
import com.jrksfa.ifive.datas.models.requests.GeoLocation;
import com.jrksfa.ifive.datas.models.requests.GeoLocationRequest;
import com.jrksfa.ifive.datas.models.responses.GeoLocationResponse;
import com.jrksfa.ifive.datas.remote.UserAPICall;
import com.jrksfa.ifive.engine.JRKEngine;
import com.jrksfa.ifive.engine.RetroFitEngine;
import com.jrksfa.ifive.gps.GPSTracker;
import com.jrksfa.ifive.gps.LocationMonitoringService;
import com.jrksfa.ifive.ui.base.BaseActivity;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AttendanceActivity extends BaseActivity {

    private LocationManager locationManager;
    @BindView(R.id.work_started)
    Button workStarted;
    @BindView(R.id.work_ended)
    Button workEnded;
    @BindView(R.id.start_layout)
    LinearLayout startLayout;
    @BindView(R.id.end_layout)
    LinearLayout endLayout;
    @BindView(R.id.end_time)
    TextView endTime;
    ProgressDialog pDialog;
    @BindView(R.id.start_time)
    TextView startTime;
    SessionManager sessionManager;
    Typeface typeface;
    ActionBar actionBar;
    Calendar myCalendar,endCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);
        ButterKnife.bind(this);
        typeface = JRKEngine.myInstance.getCommonTypeFace(this);
        actionBar = getSupportActionBar();
        actionBar.setTitle(JRKEngine.myInstance.getTitleSpan("Attendance",this));
        myCalendar = Calendar.getInstance();
        endCalendar = Calendar.getInstance();
        sessionManager =new SessionManager();
        pDialog = JRKEngine.myInstance.getProgDialog(this);
        setAttendanceLayout();
        getAttendanceStatus();
    }


    private void getAttendanceStatus() {
        pDialog.show();
        UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
        Call<AttendanceResponse> callEnqueue = userAPICall.getAttendanceStatus(sessionManager.getToken(this));
        callEnqueue.enqueue(new Callback<AttendanceResponse>() {
            @Override
            public void onResponse(Call<AttendanceResponse> call, Response<AttendanceResponse> response) {
                if(response.body()!=null){
                    setMyLayout(response.body());
                }
                if ((pDialog != null) && pDialog.isShowing())
                    pDialog.dismiss();
            }
            @Override
            public void onFailure(Call<AttendanceResponse> call, Throwable t) {
                if ((pDialog != null) && pDialog.isShowing())
                    pDialog.dismiss();
            }
        });
    }

    private void setMyLayout(final AttendanceResponse body) {
        @SuppressLint("StaticFieldLeak")
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                if(body.getWorkDetails()!=null){
                    if(body.getWorkDetails().getSTimestamp()!=null){
                        sessionManager.setStartEmployeeWorkTime(body.getWorkDetails().getSTimestamp(),
                                AttendanceActivity.this);
                    }else{
                        sessionManager.setStartEmployeeNull(AttendanceActivity.this);
                    }
                    if(body.getWorkDetails().getETimestamp()!=null){
                        sessionManager.setEndEmployeeWorkTime(body.getWorkDetails().getETimestamp(),
                                AttendanceActivity.this);
                        /*if(myCalendar.before(endCalendar)){

                        }else{
                            sessionManager.setEndEmployeeNull(AttendanceActivity.this);
                        }*/
                    }else{
                        sessionManager.setEndEmployeeNull(AttendanceActivity.this);
                    }
                }
                return null;
            }
            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                setAttendanceLayout();
            }
        };
        asyncTask.execute();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void setAttendanceLayout() {
        if(sessionManager.getStartTime(this)==null){
            if(isMyServiceRunning(LocationMonitoringService.class)){
                stopService(new Intent(this,LocationMonitoringService.class));
            }
            startLayout.setVisibility(View.GONE);
            endLayout.setVisibility(View.GONE);
            workStarted.setBackground(getResources().getDrawable(R.drawable.skyblue_button));
            workStarted.setTextColor(getResources().getColor(R.color.white));
            workEnded.setBackground(getResources().getDrawable(R.drawable.skyblue_button));
            workEnded.setTextColor(getResources().getColor(R.color.white));
        }else{
            if(!isMyServiceRunning(LocationMonitoringService.class)){
                startService(new Intent(this,LocationMonitoringService.class));
            }
            workStarted.setBackground(getResources().getDrawable(R.drawable.gray_button));
            workStarted.setTextColor(getResources().getColor(R.color.lite_grey));
            startLayout.setVisibility(View.VISIBLE);
            myCalendar.setTime(JRKEngine.myInstance.getStringToCalendar(sessionManager.getStartTime(this)));
            startTime.setText(JRKEngine.myInstance.getCalenderTimeNoS2(myCalendar));
            if(sessionManager.getEndTime(this)!=null){
                endCalendar.setTime(JRKEngine.myInstance.getStringToCalendar(sessionManager.getEndTime(this)));
                if(myCalendar.before(endCalendar)){
                    if(isMyServiceRunning(LocationMonitoringService.class)){
                        stopService(new Intent(this,LocationMonitoringService.class));
                    }
                    endLayout.setVisibility(View.VISIBLE);
                    endTime.setText(JRKEngine.myInstance.getCalenderTimeNoS2(endCalendar));
                    workEnded.setBackground(getResources().getDrawable(R.drawable.gray_button));
                    workEnded.setTextColor(getResources().getColor(R.color.lite_grey));
                }else{
                    if(!isMyServiceRunning(LocationMonitoringService.class)){
                        startService(new Intent(this,LocationMonitoringService.class));
                    }
                    endLayout.setVisibility(View.GONE);
                    workEnded.setBackground(getResources().getDrawable(R.drawable.skyblue_button));
                    workEnded.setTextColor(getResources().getColor(R.color.white));
                }
            }else{
                endLayout.setVisibility(View.GONE);
            }
        }
    }

    @OnClick(R.id.work_started)
    public void workStarted(){
        if(JRKEngine.myInstance.isNetworkAvailable(this)) {
            if(sessionManager.getStartTime(this)==null){
                updateStartedWork();
            }else{
                alertConfirmation();
            }
        }else {
            JRKEngine.myInstance.snackbarNoInternet(this);
        }
    }

    private void alertConfirmation() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Already Work Started");
        alertDialog.setMessage("DO you want to change the start work?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                updateStartedWork();
            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    private void updateStartedWork() {
        myCalendar = Calendar.getInstance();
        sessionManager.setStartEmployeeWorkTime(JRKEngine.myInstance.getCalenderTime(myCalendar), this);
        startTime.setText(JRKEngine.myInstance.getCalenderTimeNoS(myCalendar));
        setAttendanceLayout();
        updateWorkStatus(true);
    }

    private void updateWorkStatus(boolean workStatus) {
        GeoLocationRequest geoLocationRequest = new GeoLocationRequest();
        geoLocationRequest.setGeoLocation(getLatLonFromGPS());
        geoLocationRequest.setTracking(false);
        geoLocationRequest.setBattery(getBatteryPercentage(this));
        geoLocationRequest.setWork(workStatus);
        UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
        Call<GeoLocationResponse> callEnqueue = userAPICall.updateGeoLocation(sessionManager.getToken(this), geoLocationRequest);
        callEnqueue.enqueue(new Callback<GeoLocationResponse>() {
            @Override
            public void onResponse(Call<GeoLocationResponse> call, Response<GeoLocationResponse> response) {
            }

            @Override
            public void onFailure(Call<GeoLocationResponse> call, Throwable t) {
            }
        });

    }

    public static int getBatteryPercentage(Context context) {

        IntentFilter iFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, iFilter);

        int level = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) : -1;
        int scale = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1) : -1;

        float batteryPct = level / (float) scale;

        return (int) (batteryPct * 100);
    }

    @OnClick(R.id.work_ended)
    public void workEnded(){
        if(JRKEngine.myInstance.isNetworkAvailable(this)) {
            if(sessionManager.getEndTime(this)==null){
                updateEndWork();
            }else{
                if(myCalendar.before(endCalendar)){
                    alertEndConfirmation();
                }else{
                    updateEndWork();
                }
            }
        } else {
            JRKEngine.myInstance.snackbarNoInternet(this);
        }
    }

    private void alertEndConfirmation() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Already Work Ended");
        alertDialog.setMessage("DO you want to change the end work?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                updateEndWork();
            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    private void updateEndWork() {
        endCalendar = Calendar.getInstance();
        sessionManager.setEndEmployeeWorkTime(JRKEngine.myInstance.getCalenderTime(endCalendar),this);
        if(myCalendar.before(endCalendar)){
            endLayout.setVisibility(View.GONE);
        }else{
            endLayout.setVisibility(View.VISIBLE);
            endTime.setText(JRKEngine.myInstance.getCalenderTimeNoS(myCalendar));
        }
        setAttendanceLayout();
        updateWorkStatus(false);
    }

    public GeoLocation getLatLonFromGPS() {
        GeoLocation gpsLatLong = new GeoLocation();
        gpsLatLong.setLatitude(0.0);
        gpsLatLong.setLongitude(0.0);
        Location lastKnownGPSLocation;
        Location lastKnownNetworkLocation;
        String gpsLocationProvider = LocationManager.GPS_PROVIDER;
        String networkLocationProvider = LocationManager.NETWORK_PROVIDER;
        try {
            locationManager = (LocationManager) MyApplication.getAppContext().getSystemService(Context.LOCATION_SERVICE);
            lastKnownGPSLocation = locationManager.getLastKnownLocation(gpsLocationProvider);
            lastKnownNetworkLocation = locationManager.getLastKnownLocation(networkLocationProvider);
            if (lastKnownGPSLocation != null) {
                gpsLatLong.setLatitude(lastKnownGPSLocation.getLatitude());
                gpsLatLong.setLongitude(lastKnownGPSLocation.getLongitude());
                return gpsLatLong;
            } else if (lastKnownNetworkLocation != null) {
                gpsLatLong.setLatitude(lastKnownNetworkLocation.getLatitude());
                gpsLatLong.setLongitude(lastKnownNetworkLocation.getLongitude());
                return gpsLatLong;
            } else {
                return getLatLonFromGPS();
            }
        } catch (SecurityException sec) {
            return gpsLatLong;
        }
    }
}
