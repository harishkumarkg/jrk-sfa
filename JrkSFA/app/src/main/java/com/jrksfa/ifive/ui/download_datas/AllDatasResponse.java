package com.jrksfa.ifive.ui.download_datas;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jrksfa.ifive.datas.models.requests.GeoLocation;
import com.jrksfa.ifive.datas.realm_models.AllDegree;
import com.jrksfa.ifive.datas.realm_models.AllDoctor;
import com.jrksfa.ifive.datas.realm_models.AllEmployee;
import com.jrksfa.ifive.datas.realm_models.AllGrade;
import com.jrksfa.ifive.datas.realm_models.AllProduct;
import com.jrksfa.ifive.datas.realm_models.AllSpecialization;
import com.jrksfa.ifive.datas.realm_models.AllState;
import com.jrksfa.ifive.datas.realm_models.AllTown;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

public class AllDatasResponse extends RealmObject {

    @SerializedName("all_states")
    @Expose
    private RealmList<AllState> allStates = null;
    @SerializedName("all_towns")
    @Expose
    private RealmList<AllTown> allTowns = null;
    @SerializedName("all_doctors")
    @Expose
    private RealmList<AllDoctor> allDoctors = null;
    @SerializedName("all_products")
    @Expose
    private RealmList<AllProduct> allProducts = null;

    @SerializedName("all_employees")
    @Expose
    private RealmList<AllEmployee> allEmployees = null;

    @SerializedName("all_degree")
    @Expose
    private RealmList<AllDegree> allDegrees = null;

    @SerializedName("all_grade")
    @Expose
    private RealmList<AllGrade> allGrades = null;

    @SerializedName("all_specialization")
    @Expose
    private RealmList<AllSpecialization> allSpecializations = null;

    public RealmList<AllEmployee> getAllEmployees() {
        return allEmployees;
    }

    public void setAllEmployees(RealmList<AllEmployee> allEmployees) {
        this.allEmployees = allEmployees;
    }

    public RealmList<AllProduct> getAllProducts() {
        return allProducts;
    }

    public void setAllProducts(RealmList<AllProduct> allProducts) {
        this.allProducts = allProducts;
    }

    public RealmList<AllDoctor> getAllDoctors() {
        return allDoctors;
    }

    public void setAllDoctors(RealmList<AllDoctor> allDoctors) {
        this.allDoctors = allDoctors;
    }

    public RealmList<AllState> getAllStates() {
        return allStates;
    }

    public void setAllStates(RealmList<AllState> allStates) {
        this.allStates = allStates;
    }

    public RealmList<AllTown> getAllTowns() {
        return allTowns;
    }

    public void setAllTowns(RealmList<AllTown> allTowns) {
        this.allTowns = allTowns;
    }

    public RealmList<AllDegree> getAllDegree() {
        return allDegrees;
    }

    public void setAllDegree(RealmList<AllDegree> allDegree) {
        this.allDegrees = allDegree;
    }

    public RealmList<AllGrade> getAllGrade() {
        return allGrades;
    }

    public void setAllGrade(RealmList<AllGrade> allGrade) {
        this.allGrades = allGrade;
    }

    public RealmList<AllSpecialization> getAllSpecialization() {
        return allSpecializations;
    }

    public void setAllSpecialization(RealmList<AllSpecialization> allSpecialization) {
        this.allSpecializations = allSpecialization;
    }
}
