package com.jrksfa.ifive.gps;

/**
 * Created by Comp11 on 1/6/2018.
 */

public class Constants {

    public static final int LOCATION_INTERVAL = 60*1000*30;
    public static final int FASTEST_LOCATION_INTERVAL = 60*1000*30;
}