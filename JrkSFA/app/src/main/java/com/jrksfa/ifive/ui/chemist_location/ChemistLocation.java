package com.jrksfa.ifive.ui.chemist_location;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.EditText;

import com.jrksfa.ifive.R;
import com.jrksfa.ifive.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChemistLocation extends BaseActivity {

    @BindView(R.id.docLocationArea)
    Button docLocationArea;
    @BindView(R.id.docSearch)
    EditText docSearch;
    @BindView(R.id.doc_list)
    RecyclerView docList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doctor_location);
        ButterKnife.bind(this);
    }
}
