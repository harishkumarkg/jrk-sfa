package com.jrksfa.ifive.ui.doctor_dcr.fragment;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.jrksfa.ifive.R;
import com.jrksfa.ifive.adapter.spinner_adapter.GiftProductAdapter;
import com.jrksfa.ifive.datas.SessionManager;
import com.jrksfa.ifive.datas.models.responses.GiftSampleResponse;
import com.jrksfa.ifive.datas.realm_models.AllDoctor;
import com.jrksfa.ifive.datas.realm_models.AllEmployee;
import com.jrksfa.ifive.datas.realm_models.AllProduct;
import com.jrksfa.ifive.datas.realm_models.AllTown;
import com.jrksfa.ifive.datas.remote.UserAPICall;
import com.jrksfa.ifive.engine.JRKEngine;
import com.jrksfa.ifive.engine.RetroFitEngine;
import com.jrksfa.ifive.ui.doctor_dcr.DoctorDCRActivity;
import com.jrksfa.ifive.ui.doctor_dcr.adapters.add.DoctorDisplayListAdapter;
import com.jrksfa.ifive.ui.doctor_dcr.adapters.add.FocusProductDisplayListAdapter;
import com.jrksfa.ifive.ui.doctor_dcr.adapters.add.OtherProductDisplayListAdapter;
import com.jrksfa.ifive.ui.doctor_dcr.adapters.add.TownFromDisplayListAdapter;
import com.jrksfa.ifive.ui.doctor_dcr.adapters.add.TownToDisplayListAdapter;
import com.jrksfa.ifive.ui.doctor_dcr.adapters.add.VisitWithDisplayListAdapter;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AddDoctorDCRFragment extends Fragment {

    @BindView(R.id.below)
    Button below;
    @BindView(R.id.div_reason)
    EditText reason;
    @BindView(R.id.tpDate)
    TextView tpDate;
    @BindView(R.id.giftCount)
    TextView giftCount;
    @BindView(R.id.tpDateImage)
    ImageView tpDateImage;
    @BindView(R.id.div)
    RadioButton div;
    @BindView(R.id.select_doctor)
    Button selectDoctor;
    @BindView(R.id.from_area)
    Button fromArea;
    @BindView(R.id.focus_product)
    Button focusProduct;
    @BindView(R.id.other_product)
    Button otherProduct;
    @BindView(R.id.to_area)
    Button toArea;
    @BindView(R.id.visit_with)
    Button visitWith;
    @BindView(R.id.addit)
    RadioButton addit;
    @BindView(R.id.choice)
    RadioGroup choice;
    @BindView(R.id.dc_grp)
    RadioGroup dcrGroup;
    @BindView(R.id.dcr_r)
    RadioButton dcrs;
    @BindView(R.id.reminder)
    RadioButton reminder;
    @BindView(R.id.dcr_choice)
    RadioGroup dcrChoice;
    @BindView(R.id.morning)
    RadioButton morning;
    @BindView(R.id.afterNoon)
    RadioButton afternoon;
    @BindView(R.id.eve)
    RadioButton eveining;
    @BindView(R.id.giftDetails)
    RadioButton gift;
    @BindView(R.id.giftSamp)
    LinearLayout samp;
    @BindView(R.id.sampleProduct)
    RadioButton sample_product;
    @BindView(R.id.giftSample)
    RadioButton gift_sample;
    @BindView(R.id.pob_det)
    RadioButton pobDetails;
    @BindView(R.id.pobGroup)
    RadioGroup pob_group;
    @BindView(R.id.directGroup)
    RadioGroup direct_group;
    @BindView(R.id.direct)
    RadioButton Direct;
    @BindView(R.id.qty)
    RadioButton Qty;
    @BindView(R.id.mrpGroup)
    RadioGroup mrp_group;
    @BindView(R.id.mrp)
    RadioButton Mrp;
    @BindView(R.id.pts)
    RadioButton Pts;
    @BindView(R.id.ptr)
    RadioButton ptr;
    @BindView(R.id.lump_grp)
    LinearLayout lumpGrp;

    @BindView(R.id.topPanel)
    LinearLayout topPanel;
    Unbinder unbinder;
    Realm realm;
    DoctorDCRActivity activity;
    AlertDialog.Builder chartDialog;
    AlertDialog chartAlertDialog;
    View rootView;
    SessionManager sessionManager;
    private int fromTownID;
    RecyclerView.LayoutManager mLayoutManager;
    int toTownID,doctorID,visitWithID;
    List<AllProduct> focusProductID,otherProductID;
    DatePickerDialog.OnDateSetListener startDatePicker;
    Calendar myCalendar;
    private GiftSampleResponse giftSampleResponses;
    ProgressDialog pDialog;
    int sampleid = 0;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (DoctorDCRActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_add_doctor_dcr, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        realm = Realm.getDefaultInstance();
        myCalendar = Calendar.getInstance();
        tpDate.setText(JRKEngine.myInstance.getSimpleCalenderDate(myCalendar));
        startDatePicker = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                tpDate.setText(JRKEngine.myInstance.getSimpleCalenderDate(myCalendar));
            }
        };
        pDialog = JRKEngine.myInstance.getProgDialog(getActivity());;
        focusProductID = new ArrayList<>();
        otherProductID= new ArrayList<>();
        sessionManager = new SessionManager();
        giftSamplesAPI();
        return rootView;
    }

    @OnClick(R.id.from_area)
    public void fromArea(){
        View addItemView = LayoutInflater.from(activity)
                .inflate(R.layout.autosearch_recycler, null);
        chartDialog = new AlertDialog.Builder(activity);
        chartDialog.setView(addItemView);
        chartAlertDialog = chartDialog.show();
        chartAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RecyclerView townsDataList =  addItemView.findViewById(R.id.items_data_list);
        final EditText search_type =  addItemView.findViewById(R.id.search_type);
        TextView textTitle =  addItemView.findViewById(R.id.text_title);
        textTitle.setText("From Area");
        final TownFromDisplayListAdapter itemShowAdapter = new TownFromDisplayListAdapter(activity,
                realm.copyFromRealm(realm.where(AllTown.class).findAll()),
                this, fromTownID);
        townsDataList.setAdapter(itemShowAdapter);
        mLayoutManager = new LinearLayoutManager(activity);
        townsDataList.setLayoutManager(mLayoutManager);
        townsDataList.setItemAnimator(new DefaultItemAnimator());
        search_type.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = search_type.getText().toString().toLowerCase(Locale.getDefault());
                itemShowAdapter.filter(text);
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });
    }

    @OnClick(R.id.to_area)
    public void toArea(){
        View addItemView = LayoutInflater.from(activity)
                .inflate(R.layout.autosearch_recycler, null);
        chartDialog = new AlertDialog.Builder(activity);
        chartDialog.setView(addItemView);
        chartAlertDialog = chartDialog.show();
        chartAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RecyclerView townsDataList =  addItemView.findViewById(R.id.items_data_list);
        final EditText search_type =  addItemView.findViewById(R.id.search_type);
        TextView textTitle =  addItemView.findViewById(R.id.text_title);
        textTitle.setText("To Area");
        final TownToDisplayListAdapter itemShowAdapter = new TownToDisplayListAdapter(activity,
                realm.copyFromRealm(realm.where(AllTown.class).findAll()),
                this, toTownID);
        townsDataList.setAdapter(itemShowAdapter);
        mLayoutManager = new LinearLayoutManager(activity);
        townsDataList.setLayoutManager(mLayoutManager);
        townsDataList.setItemAnimator(new DefaultItemAnimator());
        search_type.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = search_type.getText().toString().toLowerCase(Locale.getDefault());
                itemShowAdapter.filter(text);
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });
    }

    @OnClick(R.id.select_doctor)
    public void selectDoctor(){
        View addItemView = LayoutInflater.from(activity)
                .inflate(R.layout.autosearch_recycler, null);
        chartDialog = new AlertDialog.Builder(activity);
        chartDialog.setView(addItemView);
        chartAlertDialog = chartDialog.show();
        chartAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RecyclerView townsDataList =  addItemView.findViewById(R.id.items_data_list);
        final EditText search_type =  addItemView.findViewById(R.id.search_type);
        TextView textTitle =  addItemView.findViewById(R.id.text_title);
        textTitle.setText("Doctors");
        final DoctorDisplayListAdapter itemShowAdapter = new DoctorDisplayListAdapter(activity,
                realm.copyFromRealm(realm.where(AllDoctor.class).findAll()),
                this, doctorID);
        townsDataList.setAdapter(itemShowAdapter);
        mLayoutManager = new LinearLayoutManager(activity);
        townsDataList.setLayoutManager(mLayoutManager);
        townsDataList.setItemAnimator(new DefaultItemAnimator());
        search_type.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = search_type.getText().toString().toLowerCase(Locale.getDefault());
                itemShowAdapter.filter(text);
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });
    }

    @OnClick(R.id.focus_product)
    public void focusProduct(){
        View addItemView = LayoutInflater.from(activity)
                .inflate(R.layout.autosearch_multi_recycler, null);
        chartDialog = new AlertDialog.Builder(activity);
        chartDialog.setView(addItemView);
        chartAlertDialog = chartDialog.show();
        chartAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RecyclerView townsDataList =  addItemView.findViewById(R.id.items_data_list);
        final EditText search_type =  addItemView.findViewById(R.id.search_type);
        Button saveButton = addItemView.findViewById(R.id.save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chartAlertDialog.dismiss();
            }
        });
        TextView textTitle =  addItemView.findViewById(R.id.text_title);
        textTitle.setText("Focus Product");
        final FocusProductDisplayListAdapter itemShowAdapter = new FocusProductDisplayListAdapter(activity,
                realm.copyFromRealm(realm.where(AllProduct.class).findAll()),
                this, focusProductID);
        townsDataList.setAdapter(itemShowAdapter);
        mLayoutManager = new LinearLayoutManager(activity);
        townsDataList.setLayoutManager(mLayoutManager);
        townsDataList.setItemAnimator(new DefaultItemAnimator());
        search_type.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = search_type.getText().toString().toLowerCase(Locale.getDefault());
                itemShowAdapter.filter(text);
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });
    }

    @OnClick(R.id.other_product)
    public void otherProduct(){
        View addItemView = LayoutInflater.from(activity)
                .inflate(R.layout.autosearch_multi_recycler, null);
        chartDialog = new AlertDialog.Builder(activity);
        chartDialog.setView(addItemView);
        chartAlertDialog = chartDialog.show();
        chartAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RecyclerView townsDataList =  addItemView.findViewById(R.id.items_data_list);
        final EditText search_type =  addItemView.findViewById(R.id.search_type);
        TextView textTitle =  addItemView.findViewById(R.id.text_title);
        textTitle.setText("Other Product");
        Button saveButton = addItemView.findViewById(R.id.save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chartAlertDialog.dismiss();
            }
        });
        final OtherProductDisplayListAdapter itemShowAdapter = new OtherProductDisplayListAdapter(activity,
                realm.copyFromRealm(realm.where(AllProduct.class).findAll()),
                this, otherProductID);
        townsDataList.setAdapter(itemShowAdapter);
        mLayoutManager = new LinearLayoutManager(activity);
        townsDataList.setLayoutManager(mLayoutManager);
        townsDataList.setItemAnimator(new DefaultItemAnimator());
        search_type.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = search_type.getText().toString().toLowerCase(Locale.getDefault());
                itemShowAdapter.filter(text);
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });
    }

    @OnClick(R.id.visit_with)
    public void visitWith(){
        View addItemView = LayoutInflater.from(activity)
                .inflate(R.layout.autosearch_recycler, null);
        chartDialog = new AlertDialog.Builder(activity);
        chartDialog.setView(addItemView);
        chartAlertDialog = chartDialog.show();
        chartAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RecyclerView townsDataList =  addItemView.findViewById(R.id.items_data_list);
        final EditText search_type =  addItemView.findViewById(R.id.search_type);
        TextView textTitle =  addItemView.findViewById(R.id.text_title);
        textTitle.setText("Visited With");
        final VisitWithDisplayListAdapter itemShowAdapter = new VisitWithDisplayListAdapter(activity,
                realm.copyFromRealm(realm.where(AllEmployee.class).findAll()),
                this, fromTownID);
        townsDataList.setAdapter(itemShowAdapter);
        mLayoutManager = new LinearLayoutManager(activity);
        townsDataList.setLayoutManager(mLayoutManager);
        townsDataList.setItemAnimator(new DefaultItemAnimator());
        search_type.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = search_type.getText().toString().toLowerCase(Locale.getDefault());
                itemShowAdapter.filter(text);
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });
    }

    public void setSaleToTownPreference(int id, String name) {
        dismissAlert();
        toTownID = id;
        toArea.setText(name);
    }

    public void setSaleFromTownPreference(int id, String name) {
        dismissAlert();
        fromTownID = id;
        fromArea.setText(name);
    }

    public void setSaleDoctorPreference(int id, String name) {
        dismissAlert();
        doctorID = id;
        selectDoctor.setText(name);
    }

    public void setGiftCount(){
        int giftCounting = 0;
        for(GiftSample sample:giftSampleResponses.getGiftSamples()){
            if(sample.getRequestedQuantity()!=null&&sample.getRequestedQuantity()!=0){
                giftCounting++;
            }
        }
        giftCount.setText(""+giftCounting);
    }


    public void setSaleFocusProductPreference(AllProduct allProduct,boolean checked) {
        if(checked){
            if(!JRKEngine.myInstance.hasProductArrayObject(focusProductID,allProduct)){
                focusProductID.add(allProduct);
            }
        }else{
            for (AllProduct product:focusProductID){
                if (product.getProductId() == allProduct.getProductId()){
                    focusProductID.remove(product);
                }
            }
        }
        focusProduct.setText(JRKEngine.myInstance.getProductNamesArray(focusProductID));
    }

    public void setSaleVisitWithPreference(int id, String name) {
        dismissAlert();
        visitWithID = id;
        visitWith.setText(name);
    }

    public void setSaleOtherProductPreference(AllProduct allProduct,boolean checked) {
        if(checked){
            if(!JRKEngine.myInstance.hasProductArrayObject(otherProductID,allProduct)){
                otherProductID.add(allProduct);
            }
        }else{
            for (AllProduct product:otherProductID){
                if (product.getProductId() == allProduct.getProductId()){
                    otherProductID.remove(product);
                }
            }
        }
        otherProduct.setText(JRKEngine.myInstance.getProductNamesArray(otherProductID));
    }

    public void dismissAlert(){
        if(chartAlertDialog != null && chartAlertDialog.isShowing()){
            chartAlertDialog.dismiss();
        }
    }

    @OnClick(R.id.below)
    public void submit(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Please select your option");
        builder.setCancelable(true);
        builder.setNegativeButton("CANCEL", null);
        builder.setPositiveButton("SUBMIT NOW", null);
        builder.show();
    }

    @OnClick({R.id.div, R.id.addit})
    public void check(RadioButton radioButton){
        boolean checked = radioButton.isChecked();
        switch(radioButton.getId()){
            case R.id.div:
                reason.setVisibility(View.VISIBLE);
                break;
            case R.id.addit:
                reason.setVisibility(View.GONE);
                break;
        }
    }

    @OnClick({R.id.giftDetails})
    public void checkd(RadioButton radioButton){
        boolean checked = radioButton.isChecked();
        switch(radioButton.getId()){
            case R.id.giftDetails:
                samp.setVisibility(View.VISIBLE);
                break;
        }
    }

    @OnClick({R.id.dcr_r, R.id.reminder})
    public void checks(RadioButton radioButton){
        boolean checked = radioButton.isChecked();
        switch(radioButton.getId()){
            case R.id.dcr_r:
                dcrChoice.setVisibility(View.VISIBLE);
                break;
            case R.id.reminder:
                dcrChoice.setVisibility(View.GONE);
                break;
        }
    }

    public void giftSamplesAPI(){
        if (JRKEngine.isNetworkAvailable(getActivity())) {
            pDialog.show();
            UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
            Call<GiftSampleResponse> callEnqueue = userAPICall.giftSamples(sessionManager.getToken(getActivity()));
            callEnqueue.enqueue(new Callback<GiftSampleResponse>() {
                @Override
                public void onResponse(Call<GiftSampleResponse> call, Response<GiftSampleResponse> response) {
                    if (response.body() != null) {
                        giftSampleResponses = response.body();
                    }else{
                        Toast.makeText(getContext(), "No Gift Samples", Toast.LENGTH_SHORT).show();
                    }
                    pDialog.dismiss();
                }
                @Override
                public void onFailure(Call<GiftSampleResponse> call, Throwable t) {
                    Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    pDialog.dismiss();
                }
            });
        } else {
            JRKEngine.myInstance.snackbarNoInternet(getActivity());
        }
    }

    @OnClick({R.id.pob_det})
    public void pob(RadioButton radioButton){
        boolean checked = radioButton.isChecked();
        switch(radioButton.getId()){
            case R.id.pob_det:
                pob_group.setVisibility(View.VISIBLE);
                break;
        }
    }

    @OnClick({R.id.productWise, R.id.lumpsum})
    public void prod(RadioButton radioButton){
        boolean checked = radioButton.isChecked();
        switch(radioButton.getId()){
            case R.id.productWise:
                direct_group.setVisibility(View.VISIBLE);
                lumpGrp.setVisibility(View.GONE);
                break;
            case R.id.lumpsum:
                direct_group.setVisibility(View.GONE);
                lumpGrp.setVisibility(View.VISIBLE);
                break;
        }
    }

    @OnClick({R.id.direct, R.id.qty})
    public void direct(RadioButton radioButton){
        boolean checked = radioButton.isChecked();
        switch(radioButton.getId()){
            case R.id.direct:
                mrp_group.setVisibility(View.GONE);
                break;
            case R.id.qty:
                mrp_group.setVisibility(View.VISIBLE);
                break;
        }
    }


    @OnClick(R.id.tpDateImage)
    public void dateee(){
        new DatePickerDialog(activity, startDatePicker, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void ShowAlertDialogWithListview()
    {
        List<String> mAnimals = new ArrayList<String>();
        mAnimals.add("Cat");
        mAnimals.add("Dog");
        mAnimals.add("Horse");
        mAnimals.add("Elephant");
        mAnimals.add("Rat");
        mAnimals.add("Lion");
        //Create sequence of items
        final CharSequence[] Animals = mAnimals.toArray(new String[mAnimals.size()]);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setTitle("Animals");
        dialogBuilder.setItems(Animals, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                String selectedText = Animals[item].toString();  //Selected item in listview
            }
        });
        //Create alert dialog object via builder
        AlertDialog alertDialogObject = dialogBuilder.create();
        //Show the dialog
        alertDialogObject.show();
    }


    @OnClick({R.id.sampleProduct, R.id.giftSample})
    public void getGiftList() {
        View addItemView = LayoutInflater.from(getActivity())
                .inflate(R.layout.giftproduct, null);
        chartDialog = new AlertDialog.Builder(getActivity());
        chartDialog.setView(addItemView);
        chartAlertDialog = chartDialog.show();
        chartAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RecyclerView giftList = addItemView.findViewById(R.id.productList);
        TextView textTitle = addItemView.findViewById(R.id.text_titles);
        Button doneButton = addItemView.findViewById(R.id.done_button);
        final EditText search_type = addItemView.findViewById(R.id.search_type);
        textTitle.setText("Product List");
        final GiftProductAdapter itemShowAdapter = new GiftProductAdapter(getActivity(), giftSampleResponses.getGiftSamples(), this, sampleid);
        giftList.setAdapter(itemShowAdapter);
        mLayoutManager = new LinearLayoutManager(getActivity());
        giftList.setLayoutManager(mLayoutManager);
        giftList.setItemAnimator(new DefaultItemAnimator());
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chartAlertDialog.dismiss();
            }
        });
    }
}
