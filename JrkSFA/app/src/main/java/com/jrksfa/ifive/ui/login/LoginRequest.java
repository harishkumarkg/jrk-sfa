package com.jrksfa.ifive.ui.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jrksfa.ifive.datas.models.requests.GeoLocation;

/**
 * Created by Comp11 on 1/25/2018.
 */

public class LoginRequest {

    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("imei")
    @Expose
    private String imei;
    @SerializedName("battery")
    @Expose
    private Integer battery;
    @SerializedName("geo_location")
    @Expose
    private GeoLocation geoLocation;

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public Integer getBattery() {
        return battery;
    }

    public void setBattery(Integer battery) {
        this.battery = battery;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public GeoLocation getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }


}
