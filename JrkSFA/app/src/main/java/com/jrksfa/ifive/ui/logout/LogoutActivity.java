package com.jrksfa.ifive.ui.logout;

import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;


import com.jrksfa.ifive.R;
import com.jrksfa.ifive.datas.SessionManager;
import com.jrksfa.ifive.datas.models.requests.GeoLocation;
import com.jrksfa.ifive.datas.remote.UserAPICall;
import com.jrksfa.ifive.engine.JRKEngine;
import com.jrksfa.ifive.engine.RetroFitEngine;
import com.jrksfa.ifive.gps.GPSTracker;
import com.jrksfa.ifive.gps.LocationMonitoringService;
import com.jrksfa.ifive.ui.base.BaseActivity;
import com.jrksfa.ifive.ui.login.LoginActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogoutActivity extends BaseActivity {

    SessionManager sessionManager;
    TextView logoutProcessing;
    GPSTracker gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logout);
        sessionManager = new SessionManager();
        logoutProcessing = findViewById(R.id.logout_processing);
        ValueAnimator animator = ValueAnimator.ofFloat(0.0f, 1.0f);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setInterpolator(new LinearInterpolator());
        animator.setDuration(9000L);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                final float progress = (float) animation.getAnimatedValue();
                final float width = logoutProcessing.getWidth();
                final float translationX = width * progress;
                logoutProcessing.setTranslationX(translationX);
            }
        });
        animator.start();
        logOut();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void logOut(){
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Logging Out")
                .setMessage("Are you sure you want to logout?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (JRKEngine.isNetworkAvailable(LogoutActivity.this)) {
                            sessionOut();
                        } else {
                            JRKEngine.myInstance.snackbarNoInternet(LogoutActivity.this);
                        }
                    }

                })
                .setNegativeButton("No", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        finish();
                    }
                })
                .show();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static int getBatteryPercentage(Context context) {

        IntentFilter iFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, iFilter);

        int level = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) : -1;
        int scale = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1) : -1;

        float batteryPct = level / (float) scale;

        return (int) (batteryPct * 100);
    }

    private void sessionOut() {
        sessionManager.logoutSession(this);
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private GeoLocation getLatLonFromGPS() {
        GeoLocation gpsLatLong = new GeoLocation();
        if (gps.canGetLocation()) {
            gpsLatLong.setLatitude(gps.getLatitude());
            gpsLatLong.setLongitude(gps.getLongitude());
        } else {
            gps.showSettingsAlert();
        }
        return gpsLatLong;
    }
}
