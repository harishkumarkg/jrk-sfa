package com.jrksfa.ifive.adapter.spinner_adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jrksfa.ifive.R;
import com.jrksfa.ifive.datas.SessionManager;
import com.jrksfa.ifive.engine.JRKEngine;
import com.jrksfa.ifive.ui.doctor_dcr.fragment.AddDoctorDCRFragment;
import com.jrksfa.ifive.ui.doctor_dcr.fragment.GiftSample;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Handler;

public class GiftProductAdapter extends RecyclerView.Adapter<GiftProductAdapter.MyViewHolder> {

    private Context context;
    private List<GiftSample> cartList;
    private ArrayList<GiftSample> townList;
    AddDoctorDCRFragment addDoctorDCRFragment;
    SessionManager sessionManager;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout viewForeground;
        public TextView giftName, serialNumber, g_qty;
        public MyViewHolder(View view) {

            super(view);
            giftName = view.findViewById(R.id.gift_name);
            serialNumber = view.findViewById(R.id.serial_number);
            viewForeground = view.findViewById(R.id.view_foreground);
            g_qty = view.findViewById(R.id.gif_qty);
        }
    }

    public GiftProductAdapter(Context context, List<GiftSample> cartList, AddDoctorDCRFragment addDoctorDCRFragment, int sampleid) {
        this.context = context;
        this.cartList = cartList;
        this.addDoctorDCRFragment = addDoctorDCRFragment;
        this.townList = new ArrayList<>();
        this.townList.addAll(cartList);
    }

    @Override
    public GiftProductAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.gift_list, parent, false);

        return new GiftProductAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final GiftSample item = cartList.get(position);
        holder.giftName.setText(item.getGiftName());
        holder.g_qty.setText(((item.getRequestedQuantity()==null||item.getRequestedQuantity()==0)?"":item.getRequestedQuantity())+"");
        holder.serialNumber.setText(position+1+"");
        holder.viewForeground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View dialogView = inflater.inflate(R.layout.gift_price_dialog,null);
                builder.setView(dialogView);
                TextView giftHead = dialogView.findViewById(R.id.giftHead);
                Button ok = dialogView.findViewById(R.id.btn_ok);
                Button cancel = dialogView.findViewById(R.id.btn_cancel);
                final EditText qty = dialogView.findViewById(R.id.pro_qty);
                giftHead.setText(item.getGiftName());
                qty.setText(((item.getRequestedQuantity()==null||item.getRequestedQuantity()==0)?"":item.getRequestedQuantity())+"");
                final AlertDialog dialog = builder.create();
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        item.setRequestedQuantity(JRKEngine.myInstance.convertStringToInt(qty.getText().toString()));
                        dialog.dismiss();
                        addDoctorDCRFragment.setGiftCount();
                        notifyDataSetChanged();
                    }
                });
                dialog.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        cartList.clear();
        if (charText.length() == 0) {
            cartList.addAll(townList);
        }else{
            for (GiftSample wp : townList){
                if (wp.getGiftName().toLowerCase(Locale.getDefault()).contains(charText)){
                    cartList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

}
