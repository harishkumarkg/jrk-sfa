package com.jrksfa.ifive.datas.models.requests;

import android.graphics.Bitmap;

public class UtilityModel {

    private String title;
    private int imageUrl;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(int imageUrl) {
        this.imageUrl = imageUrl;
    }

    public UtilityModel(String title, int imageUrl){

        this.title = title;
        this.imageUrl = imageUrl;
    }

}
