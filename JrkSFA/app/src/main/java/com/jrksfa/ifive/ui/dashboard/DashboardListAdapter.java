package com.jrksfa.ifive.ui.dashboard;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jrksfa.ifive.R;
import com.jrksfa.ifive.datas.SessionManager;
import com.jrksfa.ifive.ui.base.MenuItemsList;

import java.util.List;


public class DashboardListAdapter extends RecyclerView.Adapter<DashboardListAdapter.MyViewHolder> {

    private Context context;
    private List<DashboardItemsList> cartList;
    SessionManager sessionManager;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView icon;
        TextView menuName;
        CardView menuLayout;
        LinearLayout layout;
        public MyViewHolder(View view) {
            super(view);
            icon =view.findViewById(R.id.image);
            menuName =view.findViewById(R.id.menu_name);
            menuLayout =view.findViewById(R.id.menu_layout);
            layout =view.findViewById(R.id.layout);
        }
    }

    public DashboardListAdapter(Context context, List<DashboardItemsList> cartList) {
        this.context = context;
        this.cartList = cartList;
        sessionManager = new SessionManager();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dashboard_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final DashboardItemsList item = cartList.get(position);
        holder.icon.setImageDrawable(context.getResources().getDrawable(item.getIconID()));
        holder.menuName.setText(item.getMenuName());
//        holder.icon.setBackgroundColor(context.getResources().getColor(item.getColorID()));
        holder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, item.getaClass()));
            }
        });

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, item.getaClass()));
            }
        });

    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }
}