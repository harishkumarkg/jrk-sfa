package com.jrksfa.ifive.datas.remote;

import com.jrksfa.ifive.datas.models.requests.GeoLocationRequest;
import com.jrksfa.ifive.datas.models.responses.GeoLocationResponse;
import com.jrksfa.ifive.datas.models.responses.GiftSampleResponse;
import com.jrksfa.ifive.ui.attendance.AttendanceResponse;
import com.jrksfa.ifive.ui.login.LoginRequest;
import com.jrksfa.ifive.datas.models.responses.LoginResponse;
import com.jrksfa.ifive.ui.download_datas.AllDatasResponse;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface UserAPICall {

    @POST("jrk_mobile/public/api-login")
    Call<LoginResponse> login(@Body LoginRequest loginRequest);

    @GET("jrk_mobile/public/api-all-datas")
    Call<AllDatasResponse> allDatas(@Header("token") String token);

    @POST("jrk_mobile/public/api-all-gps")
    Call<GeoLocationResponse> updateBulkGeo(@Header("token") String token, @Body List<GeoLocationRequest> geoLocations);

    @GET("jrk_mobile/public/api-attendance-status")
    Call<AttendanceResponse> getAttendanceStatus(@Header("token") String token);

    @POST("jrk_mobile/public/api-geo-update")
    Call<GeoLocationResponse> updateGeoLocation(@Header("token") String token,@Body GeoLocationRequest geoLocationRequest);

    @POST("jrk_mobile/public/api-gift-samples")
    Call<GiftSampleResponse> giftSamples(@Header("token") String token);
}
