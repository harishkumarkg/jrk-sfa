package com.jrksfa.ifive.gps;

import com.jrksfa.ifive.datas.models.requests.GeoLocation;
import com.jrksfa.ifive.datas.models.requests.GeoLocationRequest;
import com.jrksfa.ifive.datas.models.responses.GeoLocationResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Comp11 on 1/6/2018.
 */
public interface API {

    @POST("jrk_mobile/public/api-geo-location")
    Call<GeoLocationResponse> updateGeoLocation(@Header("token") String token, @Body GeoLocationRequest geoLocation);
}
