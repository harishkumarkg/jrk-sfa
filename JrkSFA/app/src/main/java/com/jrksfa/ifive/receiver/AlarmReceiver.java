package com.jrksfa.ifive.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.jrksfa.ifive.ui.dashboard.DashboardActivity;

import static android.content.ContentValues.TAG;


public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        LocalData localData = new LocalData(context);
        if (intent.getAction() != null && context != null) {
            if (intent.getAction().equalsIgnoreCase(Intent.ACTION_BOOT_COMPLETED)) {
                // Set the alarm here.
                Log.d(TAG, "onReceive: BOOT_COMPLETED");
                NotificationScheduler.setReminder(context, AlarmReceiver.class,
                        localData.get_hour(), localData.get_min());
                return;
            }
        }
        //Trigger the notification
        NotificationScheduler.showNotification(context, DashboardActivity.class,
                "Came from Muktha SFA", "Started at Hour:"+localData.get_hour()+", Minutes :"+localData.get_min());
    }

}