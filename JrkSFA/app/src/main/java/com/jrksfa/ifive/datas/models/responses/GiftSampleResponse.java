package com.jrksfa.ifive.datas.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jrksfa.ifive.ui.doctor_dcr.fragment.GiftSample;

import java.util.List;

public class GiftSampleResponse {

    @SerializedName("gift_samples")
    @Expose
    private List<GiftSample> giftSamples = null;

    public List<GiftSample> getGiftSamples() {
        return giftSamples;
    }

    public void setGiftSamples(List<GiftSample> giftSamples) {
        this.giftSamples = giftSamples;
    }

}
