package com.jrksfa.ifive.ui.doctor_dcr.adapters.add;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jrksfa.ifive.R;
import com.jrksfa.ifive.datas.realm_models.AllDoctor;
import com.jrksfa.ifive.ui.doctor_dcr.fragment.AddDoctorDCRFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DoctorDisplayListAdapter extends RecyclerView.Adapter<DoctorDisplayListAdapter.MyViewHolder> {
    private Context context;
    private List<AllDoctor> cartList;
    private ArrayList<AllDoctor> allDoctors;
    AddDoctorDCRFragment addDoctorDCRFragment;
    Integer doctorID;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout viewForeground;
        public TextView doctorName, serialNumber;
        public MyViewHolder(View view) {

            super(view);
            doctorName = view.findViewById(R.id.doctor_name);
            serialNumber = view.findViewById(R.id.serial_number);
            viewForeground = view.findViewById(R.id.view_foreground);
        }
    }

    public DoctorDisplayListAdapter(Context context, List<AllDoctor> cartList, AddDoctorDCRFragment addDoctorDCRFragment, int doctorID) {
        this.context = context;
        this.cartList = cartList;
        this.addDoctorDCRFragment = addDoctorDCRFragment;
        this.doctorID = doctorID;
        this.allDoctors = new ArrayList<>();
        this.allDoctors.addAll(cartList);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.doctor_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final AllDoctor item = cartList.get(position);
        holder.doctorName.setText(item.getDoctorName());
        holder.serialNumber.setText(position+1+"");
        holder.viewForeground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addDoctorDCRFragment.setSaleDoctorPreference(item.getDoctorId(),item.getDoctorName());
            }
        });
        if((doctorID != 0) && (doctorID.equals(item.getDoctorId()))){
            holder.viewForeground.setBackground(context.getResources().getDrawable(R.drawable.blue_bg));
            holder.doctorName.setTextColor(context.getResources().getColor(R.color.white));
        }else{
            holder.viewForeground.setBackgroundColor(context.getResources().getColor(R.color.lite_grey3));
            holder.doctorName.setTextColor(context.getResources().getColor(R.color.black_low));
        }
    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        cartList.clear();
        if (charText.length() == 0) {
            cartList.addAll(allDoctors);
        }else{
            for (AllDoctor wp : allDoctors){
                if (wp.getDoctorName().toLowerCase(Locale.getDefault()).contains(charText)){
                    cartList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}