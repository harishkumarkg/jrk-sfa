package com.jrksfa.ifive.ui.dashboard;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jrksfa.ifive.R;
import com.jrksfa.ifive.datas.SessionManager;
import com.jrksfa.ifive.datas.models.requests.GeoLocationRequest;
import com.jrksfa.ifive.datas.models.responses.GeoLocationResponse;
import com.jrksfa.ifive.datas.remote.UserAPICall;
import com.jrksfa.ifive.engine.JRKEngine;
import com.jrksfa.ifive.engine.RetroFitEngine;
import com.jrksfa.ifive.gps.LocationMonitoringService;
import com.jrksfa.ifive.receiver.AlarmReceiver;
import com.jrksfa.ifive.receiver.NotificationScheduler;
import com.jrksfa.ifive.ui.chemist_dcr.ChemistDCRActivity;
import com.jrksfa.ifive.ui.logout.LogoutActivity;
import com.jrksfa.ifive.ui.target.TargetActivity;
import com.jrksfa.ifive.ui.attendance.AttendanceActivity;
import com.jrksfa.ifive.ui.doctor_dcr.DoctorDCRActivity;
import com.jrksfa.ifive.ui.expenses.ExpenseActivity;
import com.jrksfa.ifive.ui.stockist_dcr.StockistDCRActivity;
import com.jrksfa.ifive.ui.base.BaseActivity;
import com.jrksfa.ifive.ui.download_datas.DownloadDatasToDBActivity;
import com.jrksfa.ifive.ui.login.LoginActivity;
import com.jrksfa.ifive.ui.my_location.MapsActivity;
import com.jrksfa.ifive.ui.utility.UtilityActivity;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardActivity extends BaseActivity {

    @BindView(R.id.sync_detail)
    TextView syncDetail;
    @BindView(R.id.sync_data)
    ImageView syncData;
    @BindView(R.id.dashboard_menu)
    RecyclerView dashboardMenu;
    SessionManager sessionManager;
    List<PermissionDeniedResponse> permissionsDenied;
    List<DashboardItemsList> dashboardItemsList;
    DashboardListAdapter dashboardListAdapter;
    Realm realm = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        ButterKnife.bind(this);
        sessionManager= new SessionManager();
        getMenuItems();
        realm = Realm.getDefaultInstance();
    }

    @OnClick(R.id.sync_data)
    public void syncData(){
        startActivity(new Intent(DashboardActivity.this, DownloadDatasToDBActivity.class));
    }

    @OnClick(R.id.logout)
    public void logout(){
        startActivity(new Intent(DashboardActivity.this, LogoutActivity.class));
    }

    @OnClick(R.id.drawer_icon)
    public void drawerIconClick(){
        super.openDrawer();
    }


    private void getMenuItems() {
        dashboardItemsList = new ArrayList<>();
        dashboardItemsList.add(setMenuItem(DoctorDCRActivity.class,"Doctor DCR",R.drawable.doctor,null,R.color.menu4));
        dashboardItemsList.add(setMenuItem(ChemistDCRActivity.class,"Chemist DCR",R.drawable.chemist,null,R.color.menu3));
        dashboardItemsList.add(setMenuItem(StockistDCRActivity.class,"Stockist DCR",R.drawable.stockist,null,R.color.menu6));
        dashboardItemsList.add(setMenuItem(AttendanceActivity.class,"Attendance",R.drawable.attendance,null,R.color.menu7));
        dashboardItemsList.add(setMenuItem(StockistDCRActivity.class,"Gift Sample",R.drawable.gift_sample,null,R.color.menu8));
        dashboardItemsList.add(setMenuItem(ExpenseActivity.class,"Expense",R.drawable.expense,null,R.color.menu3));
        dashboardItemsList.add(setMenuItem(MapsActivity.class,"My Location",R.drawable.ic_pin_drop,null,R.color.menu3));
        dashboardItemsList.add(setMenuItem(TargetActivity.class,"Target",R.drawable.target,null,R.color.menu7));
        dashboardItemsList.add(setMenuItem(UtilityActivity.class,"Utility",R.drawable.uti,null,R.color.menu4));
        setMenuRecycler();
    }

    private DashboardItemsList setMenuItem(Class<?> className, String menuName,
                                           int iconPath, List<DashboardItemsList> subMenuItems,int colorID) {
        DashboardItemsList menuItemsList = new DashboardItemsList();
        menuItemsList.setaClass(className);
        menuItemsList.setIconID(iconPath);
        menuItemsList.setMenuName(menuName);
        menuItemsList.setColorID(colorID);
        menuItemsList.setSubMenuItemsList(subMenuItems);
        return menuItemsList;
    }

    private void setMenuRecycler() {
        dashboardListAdapter = new DashboardListAdapter(this, dashboardItemsList);
        int numberOfColumns = 2;
        dashboardMenu.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        dashboardMenu.setAdapter(dashboardListAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sessionManager.getToken(this) != null) {
            if(!sessionManager.getSync(this)){
                syncData();
            }else{
                syncDetail.setText("Last Sync : "+sessionManager.getLastSync(this));
                checkGPSPermission();
                LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE );
                if(!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
                    turnGPSOn();
                }
                /*NotificationScheduler.cancelReminder(this,AlarmReceiver.class);
                for(int i=0;i<24;i++){
                    addNotificationTiming(i,19);
                }*/
                if(!isMyServiceRunning(LocationMonitoringService.class)){
                    startService(new Intent(DashboardActivity.this,LocationMonitoringService.class));
                }
                if (JRKEngine.isNetworkAvailable(this)) {
                    if(realm.copyFromRealm(realm.where(GeoLocationRequest.class).findAll()).size()>0){
                        updateAllGeoLocation();
                    }
                }
            }
        } else {
            startActivity(new Intent(DashboardActivity.this, LoginActivity.class));
        }
    }

    private void updateAllGeoLocation() {
        sessionManager = new SessionManager();
        List<GeoLocationRequest> geoLocations = realm.copyFromRealm(realm.where(GeoLocationRequest.class).findAll());
        if(geoLocations.size()!=0){
            UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
            Call<GeoLocationResponse> callEnqueue = userAPICall.updateBulkGeo(sessionManager.getToken(this),geoLocations);
            callEnqueue.enqueue(new Callback<GeoLocationResponse>() {
                @Override
                public void onResponse(Call<GeoLocationResponse> call, Response<GeoLocationResponse> response) {
                    response.body();
                    final RealmResults<GeoLocationRequest> results = realm.where(GeoLocationRequest.class).findAll();
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            results.deleteAllFromRealm();
                        }
                    });
                    realm.copyFromRealm(realm.where(GeoLocationRequest.class).findAll());
                }
                @Override
                public void onFailure(Call<GeoLocationResponse> call, Throwable t) {
                    Toast.makeText(DashboardActivity.this,  t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void turnGPSOn(){
        Toast.makeText(this, "Please Enable GPS", Toast.LENGTH_SHORT).show();
        try{
            Intent intent1 = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent1);
        }catch (Exception e) {

        }
    }

    private void checkGPSPermission() {
        Dexter.withActivity(this)
                .withPermissions(
                        android.Manifest.permission.ACCESS_COARSE_LOCATION,
                        android.Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                        }else{
                            permissionsDenied = report.getDeniedPermissionResponses();
                        }
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                        }
                    }
                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    private void addNotificationTiming(int hour, int minute) {
        NotificationScheduler.setReminder(DashboardActivity.this,
                AlarmReceiver.class, hour, minute);
    }


}
