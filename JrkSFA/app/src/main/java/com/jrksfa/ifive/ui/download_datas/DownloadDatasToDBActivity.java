package com.jrksfa.ifive.ui.download_datas;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;
import android.widget.Toast;

import com.jrksfa.ifive.R;
import com.jrksfa.ifive.datas.SessionManager;
import com.jrksfa.ifive.datas.remote.UserAPICall;
import com.jrksfa.ifive.engine.JRKEngine;
import com.jrksfa.ifive.engine.RetroFitEngine;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.github.igla.ferriswheel.FerrisWheelView;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;

public class DownloadDatasToDBActivity extends AppCompatActivity {

    @BindView(R.id.db_processing)
    TextView dbProcessing;
    ProgressDialog pDialog;
    SessionManager sessionManager;
    ActionBar actionBar;
    @BindView(R.id.wait_msg)
    TextView waitMsg;
    Realm realm;
    @BindView(R.id.load_image)
    FerrisWheelView ferrisWheelView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_datas_to_db);
        ButterKnife.bind(this);
        getSupportActionBar().hide();
        pDialog = JRKEngine.myInstance.getProgDialog(this);
        sessionManager = new SessionManager();
        getAllDatasList();
        ferrisWheelView.startAnimation();
        Animation anim = new AlphaAnimation(0.2f, 1.0f);
        anim.setDuration(400); //You can manage the blinking time with this parameter
        anim.setStartOffset(30);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        waitMsg.startAnimation(anim);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(ContextCompat.getColor(this, R.color.login_bottom_nav));
        }
    }

    public void getAllDatasList(){
        if (JRKEngine.isNetworkAvailable(this)) {
//            pDialog.show();
            UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
            Call<AllDatasResponse> callEnqueue = userAPICall.allDatas(sessionManager.getToken(this));
            callEnqueue.enqueue(new Callback<AllDatasResponse>() {
                @Override
                public void onResponse(Call<AllDatasResponse> call, Response<AllDatasResponse> response) {
                    if(response.body()!=null){
                        uploadToRealmDB(response.body());
                    }
                }
                @Override
                public void onFailure(Call<AllDatasResponse> call, Throwable t) {
                    Toast.makeText(DownloadDatasToDBActivity.this,  t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            JRKEngine.myInstance.snackbarNoInternet(this);
        }
    }

    private void uploadToRealmDB(final AllDatasResponse body) {
        realm = Realm.getDefaultInstance();
        Observable<Integer> observable = Observable.just(1);
        observable
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Integer>() {
                    @Override public void onCompleted() {
                        realm.beginTransaction();
                        AllDatasResponse allDatasResponse = realm.copyToRealm(body);
                        realm.commitTransaction();
                        Toast.makeText(DownloadDatasToDBActivity.this, "Sync Completed.", Toast.LENGTH_SHORT).show();
                        openMainActivity();
                    }

                    @Override public void onError(Throwable e) {
                        Log.d("Test", "In onError()");
                    }

                    @Override public void onNext(Integer integer) {
//                        final RealmResults<AllDatasResponse> puppies = realm.where(AllDatasResponse.class).findAll();
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.deleteAll();
                            }
                        });
                    }
                });
    }

    private void openMainActivity() {
        sessionManager.setSync(this);
        finish();
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, "Please wait until completed", Toast.LENGTH_SHORT).show();
    }
}
