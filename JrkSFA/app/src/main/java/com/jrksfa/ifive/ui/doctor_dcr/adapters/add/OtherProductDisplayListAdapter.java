package com.jrksfa.ifive.ui.doctor_dcr.adapters.add;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jrksfa.ifive.R;
import com.jrksfa.ifive.datas.realm_models.AllProduct;
import com.jrksfa.ifive.engine.JRKEngine;
import com.jrksfa.ifive.ui.doctor_dcr.fragment.AddDoctorDCRFragment;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.Locale;

public class OtherProductDisplayListAdapter extends RecyclerView.Adapter<OtherProductDisplayListAdapter.MyViewHolder> {
    private Context context;
    private List<AllProduct> cartList;
    private ArrayList<AllProduct> allDoctors;
    AddDoctorDCRFragment addDoctorDCRFragment;
    List<AllProduct> doctorID;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout viewForeground;
        public TextView productName;
        CheckBox selectProduct;
        public MyViewHolder(View view) {

            super(view);
            productName = view.findViewById(R.id.product_name);
            selectProduct = view.findViewById(R.id.select_product);
            viewForeground = view.findViewById(R.id.view_foreground);
        }
    }

    public OtherProductDisplayListAdapter(Context context, List<AllProduct> cartList,
                                          AddDoctorDCRFragment addDoctorDCRFragment, List<AllProduct> doctorID) {
        this.context = context;
        this.cartList = cartList;
        this.addDoctorDCRFragment = addDoctorDCRFragment;
        this.doctorID = doctorID;
        this.allDoctors = new ArrayList<>();
        this.allDoctors.addAll(cartList);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_list_multi, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final AllProduct item = cartList.get(position);
        holder.productName.setText(item.getProductName());
        /*holder.viewForeground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });*/
        holder.selectProduct.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    holder.viewForeground.setBackground(context.getResources().getDrawable(R.drawable.blue_bg));
                    holder.productName.setTextColor(context.getResources().getColor(R.color.white));
                }else{
                    holder.viewForeground.setBackgroundColor(context.getResources().getColor(R.color.lite_grey3));
                    holder.productName.setTextColor(context.getResources().getColor(R.color.black_low));
                }
                try{
                    addDoctorDCRFragment.setSaleOtherProductPreference(item,isChecked);
                }catch (ConcurrentModificationException con){
                    Toast.makeText(context, "You Mobile is too Slow", Toast.LENGTH_SHORT).show();
                }
            }
        });
        if((doctorID.size() != 0) && (JRKEngine.myInstance.hasProductArrayObject(doctorID,item))){
            holder.selectProduct.setChecked(true);
            holder.viewForeground.setBackground(context.getResources().getDrawable(R.drawable.blue_bg));
            holder.productName.setTextColor(context.getResources().getColor(R.color.white));
        }else{
            holder.selectProduct.setChecked(false);
            holder.viewForeground.setBackgroundColor(context.getResources().getColor(R.color.lite_grey3));
            holder.productName.setTextColor(context.getResources().getColor(R.color.black_low));
        }
    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        cartList.clear();
        if (charText.length() == 0) {
            cartList.addAll(allDoctors);
        }else{
            for (AllProduct wp : allDoctors){
                if (wp.getProductName().toLowerCase(Locale.getDefault()).contains(charText)){
                    cartList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}