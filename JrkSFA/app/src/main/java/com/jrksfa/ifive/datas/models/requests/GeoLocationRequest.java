package com.jrksfa.ifive.datas.models.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by Comp11 on 2/1/2018.
 */

public class GeoLocationRequest extends RealmObject {
    @SerializedName("tracking")
    @Expose
    private Boolean tracking;
    @SerializedName("work")
    @Expose
    private Boolean work;
    @SerializedName("battery")
    @Expose
    private Integer battery;
    @SerializedName("geo_location")
    @Expose
    private GeoLocation geoLocation;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Boolean getWork() {
        return work;
    }

    public void setWork(Boolean work) {
        this.work = work;
    }

    public Integer getBattery() {
        return battery;
    }

    public void setBattery(Integer battery) {
        this.battery = battery;
    }

    public Boolean getTracking() {
        return tracking;
    }

    public void setTracking(Boolean tracking) {
        this.tracking = tracking;
    }

    public GeoLocation getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }

}
