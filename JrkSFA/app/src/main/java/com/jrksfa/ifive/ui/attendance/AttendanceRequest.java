package com.jrksfa.ifive.ui.attendance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Comp11 on 3/27/2018.
 */

public class AttendanceRequest {

    @SerializedName("attendance_reason_id")
    @Expose
    private Integer attendanceReasonId;
    @SerializedName("town_id")
    @Expose
    private Integer town_id;
    @SerializedName("remarks")
    @Expose
    private String remarks;

    public Integer getTown_id() {
        return town_id;
    }

    public void setTown_id(Integer town_id) {
        this.town_id = town_id;
    }

    public Integer getAttendanceReasonId() {
        return attendanceReasonId;
    }

    public void setAttendanceReasonId(Integer attendanceReasonId) {
        this.attendanceReasonId = attendanceReasonId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

}
