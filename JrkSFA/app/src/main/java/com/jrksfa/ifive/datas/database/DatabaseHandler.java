package com.jrksfa.ifive.datas.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Comp11 on 1/8/2018.
 */

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "rathna_cements";
    private static final String TABLE_GPS = "gps_location";
    private static final String KEY_ID = "id";
    private static final String KEY_UID = "uid";
    private static final String KEY_LAT = "latitude";
    private static final String KEY_LON = "longitude";
    private static final String KEY_BATTERY = "battery";
    private static final String KEY_DATE = "date";
    private static final String KEY_TIME = "time";

    private static final String TABLE_MENUS = "menu_permission";
    private static final String KEY_MENU_ID = "menu_id";
    private static final String KEY_SLUG = "slug";
    private static final String KEY_MENU = "menu_name";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //3rd argument to be passed is CursorFactory instance
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_GPS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_UID + " INTEGER," +
                KEY_LAT + " TEXT," + KEY_LON + " TEXT," + KEY_DATE + " TEXT," + KEY_TIME + " TEXT,"
                + KEY_BATTERY + " TEXT" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
        String CREATE_MENUS = "CREATE TABLE IF NOT EXISTS " + TABLE_MENUS + "("
                + KEY_MENU_ID + " INTEGER PRIMARY KEY," + KEY_SLUG + " TEXT," + KEY_MENU + " TEXT)";
        db.execSQL(CREATE_MENUS);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GPS);

        // Create tables again
        onCreate(db);
    }

}