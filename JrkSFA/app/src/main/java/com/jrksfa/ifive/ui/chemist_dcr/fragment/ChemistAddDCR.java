package com.jrksfa.ifive.ui.chemist_dcr.fragment;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.jrksfa.ifive.R;
import com.jrksfa.ifive.engine.JRKEngine;
import com.jrksfa.ifive.ui.chemist_dcr.ChemistDCRActivity;
import com.jrksfa.ifive.ui.chemist_dcr.chemist_adapter.TownChemistDisplayListAdapter;
import com.jrksfa.ifive.datas.realm_models.AllTown;

import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.Realm;

public class ChemistAddDCR extends Fragment {

    @BindView(R.id.below_smt)
    Button below;
    @BindView(R.id.tpDate)
    TextView tpDate;
    @BindView(R.id.tpDateImage)
    ImageView tpDateImage;
    @BindView(R.id.fromArea)
    Button fromArea;
    @BindView(R.id.toArea)
    Button toArea;
    @BindView(R.id.select_chemist)
    Button selectChemist;
    @BindView(R.id.topPanel)
    LinearLayout topPanel;
    private int mYear, mMonth, mDay, mHour, mMinute;
    Unbinder unbinder;
    Realm realm;
    ChemistDCRActivity activity;
    AlertDialog.Builder chartDialog;
    AlertDialog chartAlertDialog;
    View rootView;
    RecyclerView.LayoutManager mLayoutManager;
    int toTownID,doctorID,visitWithID;
    private int fromTownID;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (ChemistDCRActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_chemist_add, container, false);
        realm = Realm.getDefaultInstance();
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @OnClick(R.id.fromArea)
    public void fromArea(){
        View addItemView = LayoutInflater.from(activity)
                .inflate(R.layout.autosearch_recycler, null);
        chartDialog = new AlertDialog.Builder(activity);
        chartDialog.setView(addItemView);
        chartAlertDialog = chartDialog.show();
        chartAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RecyclerView townsDataList =  addItemView.findViewById(R.id.items_data_list);
        final EditText search_type =  addItemView.findViewById(R.id.search_type);
        TextView textTitle =  addItemView.findViewById(R.id.text_title);
        textTitle.setText("From Area");
        final TownChemistDisplayListAdapter itemShowAdapter = new TownChemistDisplayListAdapter(activity,
                realm.copyFromRealm(realm.where(AllTown.class).findAll()),
                this, fromTownID);
        townsDataList.setAdapter(itemShowAdapter);
        mLayoutManager = new LinearLayoutManager(activity);
        townsDataList.setLayoutManager(mLayoutManager);
        townsDataList.setItemAnimator(new DefaultItemAnimator());
        search_type.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = search_type.getText().toString().toLowerCase(Locale.getDefault());
                itemShowAdapter.filter(text);
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });
    }

    @OnClick(R.id.toArea)
    public void toArea(){
        View addItemView = LayoutInflater.from(activity)
                .inflate(R.layout.autosearch_recycler, null);
        chartDialog = new AlertDialog.Builder(activity);
        chartDialog.setView(addItemView);
        chartAlertDialog = chartDialog.show();
        chartAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RecyclerView townsDataList =  addItemView.findViewById(R.id.items_data_list);
        final EditText search_type =  addItemView.findViewById(R.id.search_type);
        TextView textTitle =  addItemView.findViewById(R.id.text_title);
        textTitle.setText("To Area");
        final TownChemistDisplayListAdapter itemShowAdapter = new TownChemistDisplayListAdapter(activity,
                realm.copyFromRealm(realm.where(AllTown.class).findAll()),
                this, toTownID);
        townsDataList.setAdapter(itemShowAdapter);
        mLayoutManager = new LinearLayoutManager(activity);
        townsDataList.setLayoutManager(mLayoutManager);
        townsDataList.setItemAnimator(new DefaultItemAnimator());
        search_type.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = search_type.getText().toString().toLowerCase(Locale.getDefault());
                itemShowAdapter.filter(text);
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });
    }

    public void dismissAlert(){
        if(chartAlertDialog != null && chartAlertDialog.isShowing()){
            chartAlertDialog.dismiss();
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void setSaleToTownPreference(int id, String name) {
        dismissAlert();
        toTownID = id;
        toArea.setText(name);
    }

    public void setSaleFromTownPreference(int id, String name) {
        dismissAlert();
        fromTownID = id;
        fromArea.setText(name);
    }

    public void setSaleDoctorPreference(int id, String name) {
        dismissAlert();
        doctorID = id;
       // selectDoctor.setText(name);
    }

}
