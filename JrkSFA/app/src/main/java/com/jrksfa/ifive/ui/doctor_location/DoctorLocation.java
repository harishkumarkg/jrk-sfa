package com.jrksfa.ifive.ui.doctor_location;

import android.app.AlertDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.jrksfa.ifive.R;
import com.jrksfa.ifive.adapter.spinner_adapter.AddDoctorAreaAdapter;
import com.jrksfa.ifive.datas.realm_models.AllTown;
import com.jrksfa.ifive.ui.base.BaseActivity;
import com.jrksfa.ifive.ui.doctor_location.adapter.DoctorAreaAdapter;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

public class DoctorLocation extends BaseActivity {

    @BindView(R.id.docLocationArea)
    Button docLocationArea;
    @BindView(R.id.docSearch)
    EditText docSearch;
    @BindView(R.id.doc_list)
    RecyclerView docList;
    AlertDialog.Builder chartDialog;
    AlertDialog chartAlertDialog;
    Realm realm;
    RecyclerView.LayoutManager mLayoutManager;
    private int fromTownID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doctor_location);
        realm = Realm.getDefaultInstance();
        ButterKnife.bind(this);
    }

    public void dismissAlert(){
        if(chartAlertDialog != null && chartAlertDialog.isShowing()){
            chartAlertDialog.dismiss();
        }
    }

    @OnClick(R.id.docLocationArea)
    public void fromArea(){
        View addItemView = LayoutInflater.from(this)
                .inflate(R.layout.autosearch_recycler, null);
        chartDialog = new AlertDialog.Builder(this);
        chartDialog.setView(addItemView);
        chartAlertDialog = chartDialog.show();
        chartAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RecyclerView townsDataList =  addItemView.findViewById(R.id.items_data_list);
        final EditText search_type =  addItemView.findViewById(R.id.search_type);
        TextView textTitle =  addItemView.findViewById(R.id.text_title);
        textTitle.setText("From Area");
        final DoctorAreaAdapter itemShowAdapter = new DoctorAreaAdapter(this,
                realm.copyFromRealm(realm.where(AllTown.class).findAll()),
                this, fromTownID);
        townsDataList.setAdapter(itemShowAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        townsDataList.setLayoutManager(mLayoutManager);
        townsDataList.setItemAnimator(new DefaultItemAnimator());
        search_type.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = search_type.getText().toString().toLowerCase(Locale.getDefault());
                itemShowAdapter.filter(text);
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });
    }
    public void setSaleFromTownPreference(int id, String name) {
        dismissAlert();
        fromTownID = id;
        docLocationArea.setText(name);
    }
}
