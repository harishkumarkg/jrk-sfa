package com.jrksfa.ifive.ui.doctor_dcr.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jrksfa.ifive.R;
import com.jrksfa.ifive.ui.doctor_dcr.DoctorDCRActivity;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;


public class ViewDoctorDCRFragment extends Fragment {

	View rootView;
	DoctorDCRActivity activity;
	Unbinder unbinder;
	Realm realm;

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		this.activity = (DoctorDCRActivity) context;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_doctor_view, container, false);
		unbinder = ButterKnife.bind(this, rootView);
		realm = Realm.getDefaultInstance();
		return rootView;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		unbinder.unbind();
	}
}
