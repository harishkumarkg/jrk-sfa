package com.jrksfa.ifive.ui.login;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;

import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;

import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import android.telephony.TelephonyManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jrksfa.ifive.ui.dashboard.DashboardActivity;
import com.jrksfa.ifive.R;
import com.jrksfa.ifive.datas.SessionManager;
import com.jrksfa.ifive.datas.models.requests.GeoLocation;
import com.jrksfa.ifive.datas.models.responses.LoginResponse;
import com.jrksfa.ifive.datas.remote.UserAPICall;
import com.jrksfa.ifive.engine.JRKEngine;
import com.jrksfa.ifive.engine.RetroFitEngine;
import com.jrksfa.ifive.gps.GPSTracker;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.emailID)
    EditText emailID;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.login_button)
    Button login;
    SessionManager sessionManager;
    ProgressDialog pDialog;
    GPSTracker gps;
    List<PermissionDeniedResponse> permissionsDenied;
    private String deviceNumber="1";
    LoginResponse responseMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        getSupportActionBar().hide();
        gps = new GPSTracker(this);
        sessionManager = new SessionManager();
        pDialog = JRKEngine.myInstance.getProgDialog(this);
        checkGPSPermission();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(ContextCompat.getColor(this, R.color.login_bottom_nav));
        }
        getDeviceIMEI();
    }

    private void checkGPSPermission() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            // do you work now
                        }else{
                            permissionsDenied = report.getDeniedPermissionResponses();
                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                        }
                    }
                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    private void turnGPSOn(){
        Toast.makeText(this, "Please Enable GPS", Toast.LENGTH_SHORT).show();
        try{
            Intent intent1 = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent1);
        }catch (Exception e) {

        }
    }

    @OnClick(R.id.login_button)
    public void openMain(){
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE );
        if(!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            turnGPSOn();
        }else if(JRKEngine.myInstance.isEmpty(emailID.getText().toString())) {
            emailID.setError("Username is required!");
        }else if(JRKEngine.myInstance.isEmpty(password.getText().toString())) {
            password.setError("Wrong Password!");
        }else{
            gps = new GPSTracker(this);
            LoginRequest loginRequest = new LoginRequest();
            loginRequest.setUsername(emailID.getText().toString());
            loginRequest.setPassword(password.getText().toString());
            loginAPI(loginRequest);
        }
    }


    private void loginAPI(LoginRequest loginRequest) {
        if(JRKEngine.myInstance.isNetworkAvailable(this)) {
            pDialog.show();
            responseMsg = new LoginResponse();
            loginRequest.setGeoLocation(getLatLonFromGPS());
            getDeviceIMEI();
            if(deviceNumber==null){
                loginRequest.setImei("Undefined");
            }else{
                loginRequest.setImei(deviceNumber);
            }
            loginRequest.setBattery(getBatteryPercentage(this));
            UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
            Call<LoginResponse> callEnqueue = userAPICall.login(loginRequest);
            callEnqueue.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    responseMsg = response.body();
                    if (responseMsg != null) {
                        if(response.body().getMessage().equals("Invalid Username/Password") ||
                                response.body().getMessage().equals("Expired")){

                        }else{
                            if(response.body().getAttendanceResponse().getWorkDetails()!=null){

                                if(response.body().getAttendanceResponse().getWorkDetails().getSTimestamp()!=null){
                                    sessionManager.setStartEmployeeWorkTime(response.body().getAttendanceResponse().getWorkDetails().getSTimestamp(),
                                            LoginActivity.this);
                                }else{
                                    sessionManager.setStartEmployeeNull(LoginActivity.this);
                                }
                                if(response.body().getAttendanceResponse().getWorkDetails().getETimestamp()!=null){
                                    sessionManager.setEndEmployeeWorkTime(response.body().getAttendanceResponse().getWorkDetails().getETimestamp(),
                                            LoginActivity.this);
                                }else{
                                    sessionManager.setEndEmployeeNull(LoginActivity.this);
                                }
                            }
                            if (responseMsg != null) {
                                sessionManager.setPreferences(LoginActivity.this, responseMsg);
                                if(responseMsg.getToken()!=null){
                                    Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                            }
                        }
                        Toast.makeText(LoginActivity.this,
                                "" + responseMsg.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    if ((pDialog != null) && pDialog.isShowing())
                        pDialog.dismiss();

                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    if ((pDialog != null) && pDialog.isShowing())
                        pDialog.dismiss();
                    Toast.makeText(LoginActivity.this,"Please check the ID or Password", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            JRKEngine.myInstance.snackbarNoInternet(this);
        }
    }

    private GeoLocation getLatLonFromGPS() {
        GeoLocation gpsLatLong = new GeoLocation();
        if (gps.canGetLocation()) {
            gpsLatLong.setLatitude(gps.getLatitude());
            gpsLatLong.setLongitude(gps.getLongitude());
            return gpsLatLong;
        } else {
            gps = new GPSTracker(this);
//            gps.showSettingsAlert();
            return getLatLonFromGPS();
        }
    }

    public void getDeviceIMEI() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 123);
        } else {
            TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
            deviceNumber = telephonyManager.getDeviceId();
        }
    }

    public static int getBatteryPercentage(Context context) {

        IntentFilter iFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, iFilter);

        int level = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) : -1;
        int scale = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1) : -1;

        float batteryPct = level / (float) scale;

        return (int) (batteryPct * 100);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
