package com.jrksfa.ifive.ui.attendance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jrksfa.ifive.ui.login.AttendanceStatus;

/**
 * Created by Comp11 on 2/13/2018.
 */

public class AttendanceResponse {

    @SerializedName("work_details")
    @Expose
    private AttendanceStatus workDetails;
    @SerializedName("message")
    @Expose
    private String message;

    public AttendanceStatus getWorkDetails() {
        return workDetails;
    }

    public void setWorkDetails(AttendanceStatus workDetails) {
        this.workDetails = workDetails;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
