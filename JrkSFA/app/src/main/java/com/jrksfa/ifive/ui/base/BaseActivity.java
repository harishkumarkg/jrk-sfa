package com.jrksfa.ifive.ui.base;

import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.jrksfa.ifive.ui.chemist_dcr.ChemistDCRActivity;
import com.jrksfa.ifive.ui.expenses.ExpenseActivity;
import com.jrksfa.ifive.ui.stockist_dcr.StockistDCRActivity;
import com.jrksfa.ifive.ui.target.TargetActivity;
import com.jrksfa.ifive.ui.dashboard.DashboardActivity;
import com.jrksfa.ifive.R;
import com.jrksfa.ifive.datas.SessionManager;
import com.jrksfa.ifive.ui.attendance.AttendanceActivity;
import com.jrksfa.ifive.ui.doctor_dcr.DoctorDCRActivity;
import com.jrksfa.ifive.ui.logout.LogoutActivity;
import com.jrksfa.ifive.ui.my_location.MapsActivity;

import java.util.ArrayList;
import java.util.List;


public abstract class BaseActivity extends AppCompatActivity implements MenuItem.OnMenuItemClickListener {

    private FrameLayout view_stub; //This is the framelayout to keep your content view
    private NavigationView navigation_view; // The new navigation view from Android Design Library. Can inflate menu resources. Easy
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private Menu drawerMenu;
    Typeface typeface,sfaTypeFace;
    SessionManager sessionManager;
    RecyclerView customMenuNavigation;
    MenuListAdapter menuItemListAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    List<MenuItemsList> menuItemsList;
    List<MenuItemsList> subMenuItemsList;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.drawer_layout);// The base layout that contains your navigation drawer.
        view_stub = findViewById(R.id.view_stub);
        navigation_view = findViewById(R.id.navigation_view);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        customMenuNavigation = findViewById(R.id.custom_menu_navigation);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                0, 0);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sessionManager = new SessionManager();
        drawerMenu = navigation_view.getMenu();
        for(int i = 0; i < drawerMenu.size(); i++) {
            drawerMenu.getItem(i).setOnMenuItemClickListener(this);
        }
        navigation_view.setItemIconTintList(null);
        getMenuItems();

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }

    }

    public void openDrawer() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            mDrawerLayout.openDrawer(GravityCompat.START);
        }
    }

    private void getMenuItems() {
        menuItemsList = new ArrayList<>();
        menuItemsList.add(setMenuItem(DashboardActivity.class,"Home",R.drawable.ic_home,null));
        menuItemsList.add(setMenuItem(MapsActivity.class,"My Location",R.drawable.ic_pin_drop,null));
        menuItemsList.add(setMenuItem(DoctorDCRActivity.class,"Doctor DCR",R.drawable.doctor,null));
        menuItemsList.add(setMenuItem(ChemistDCRActivity.class,"Chemist DCR",R.drawable.chemist,null));
        menuItemsList.add(setMenuItem(StockistDCRActivity.class,"Stockist DCR",R.drawable.stockist,null));
        menuItemsList.add(setMenuItem(AttendanceActivity.class,"Attendance",R.drawable.attendance,null));
        menuItemsList.add(setMenuItem(StockistDCRActivity.class,"Gift Sample",R.drawable.gift_sample,null));
        menuItemsList.add(setMenuItem(ExpenseActivity.class,"Expense",R.drawable.expense,null));
        menuItemsList.add(setMenuItem(TargetActivity.class,"Target",R.drawable.target,null));
        menuItemsList.add(setMenuItem(LogoutActivity.class,"Logout",R.drawable.ic_power_settings_new,null));
        setMenuRecycler();
    }

    private void setMenuRecycler() {
        menuItemListAdapter = new MenuListAdapter(this, menuItemsList);
        customMenuNavigation.setAdapter(menuItemListAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        customMenuNavigation.setLayoutManager(mLayoutManager);
        customMenuNavigation.setItemAnimator(new DefaultItemAnimator());
    }

    private MenuItemsList setMenuItem(Class<?> className, String menuName, int iconPath, List<MenuItemsList> subMenuItems) {
        MenuItemsList menuItemsList = new MenuItemsList();
        menuItemsList.setaClass(className);
        menuItemsList.setIconID(iconPath);
        menuItemsList.setMenuName(menuName);
        menuItemsList.setSubMenuItemsList(subMenuItems);
        return menuItemsList;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
        }
        mDrawerLayout.closeDrawers();
        return false;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    @Override
    public void setContentView(int layoutResID) {
        if (view_stub != null) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            View stubView = inflater.inflate(layoutResID, view_stub, false);
            view_stub.addView(stubView, lp);
        }
    }

    @Override
    public void setContentView(View view) {
        if (view_stub != null) {
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            view_stub.addView(view, lp);
        }
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        if (view_stub != null) {
            view_stub.addView(view, params);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}