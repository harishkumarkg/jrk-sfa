package com.jrksfa.ifive.ui.doctor_dcr;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;

import com.jrksfa.ifive.R;
import com.jrksfa.ifive.engine.JRKEngine;
import com.jrksfa.ifive.ui.doctor_dcr.fragment.AddDoctorDCRFragment;
import com.jrksfa.ifive.ui.doctor_dcr.fragment.ViewDoctorDCRFragment;
import com.jrksfa.ifive.ui.base.BaseActivity;

import butterknife.ButterKnife;


public class DoctorDCRActivity extends BaseActivity {

	private AddDoctorDCRFragment addDoctorDCRFragment;
	private ViewDoctorDCRFragment viewDoctorDCRFragment;
	private TabLayout tabLayout;
	ActionBar actionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.doctor_dcr);
		ButterKnife.bind(this);
		actionBar = getSupportActionBar();
		actionBar.setTitle(JRKEngine.myInstance.getTitleSpan("Doctor DCR",this));
		tabLayout = findViewById(R.id.tabs);
		bindWidgetsWithAnEvent();
		setupTabLayout();
	}

	private void setupTabLayout() {
		addDoctorDCRFragment = new AddDoctorDCRFragment();
		viewDoctorDCRFragment = new ViewDoctorDCRFragment();
		tabLayout.addTab(tabLayout.newTab().setText("Add DCR"),true);
		tabLayout.addTab(tabLayout.newTab().setText("View DCR"));
	}

	private void bindWidgetsWithAnEvent(){
		tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
			@Override
			public void onTabSelected(TabLayout.Tab tab) {
				setCurrentTabFragment(tab.getPosition());
			}

			@Override
			public void onTabUnselected(TabLayout.Tab tab) {
			}

			@Override
			public void onTabReselected(TabLayout.Tab tab) {
			}
		});
	}

	private void setCurrentTabFragment(int tabPosition){
		switch (tabPosition){
			case 0 :
				replaceFragment(addDoctorDCRFragment);
				break;
			case 1 :
				replaceFragment(viewDoctorDCRFragment);
				break;
		}
	}

	public void replaceFragment(Fragment fragment) {
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.replace(R.id.frame_container, fragment);
		ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		ft.commit();
	}
}
