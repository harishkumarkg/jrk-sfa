package com.jrksfa.ifive.ui.utility;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.jrksfa.ifive.R;
import com.jrksfa.ifive.adapter.UtilityAdapter;
import com.jrksfa.ifive.datas.models.requests.UtilityModel;
import com.jrksfa.ifive.ui.base.BaseActivity;

public class UtilityActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.utility_list);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.utiliyLists);
        UtilityModel itemsData[] = {new UtilityModel("Reports", R.drawable.reports),
                new UtilityModel("Add Doctor", R.drawable.adddoctor),
                new UtilityModel("Update Doctor Location", R.drawable.updatedoctor),
                new UtilityModel("Update Chemist Location", R.drawable.updatechemist),
                new UtilityModel("Update Stockist Location", R.drawable.updatestockist)};




                recyclerView.setLayoutManager(new LinearLayoutManager(this));

        UtilityAdapter mAdapter = new UtilityAdapter(itemsData);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());




    }
}
