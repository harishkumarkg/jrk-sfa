package com.jrksfa.ifive.datas.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Comp11 on 2/1/2018.
 */

public class GeoLocationResponse {
    @SerializedName("employee_log_id")
    @Expose
    private Integer employeeLogId;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getEmployeeLogId() {
        return employeeLogId;
    }

    public void setEmployeeLogId(Integer employeeLogId) {
        this.employeeLogId = employeeLogId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
