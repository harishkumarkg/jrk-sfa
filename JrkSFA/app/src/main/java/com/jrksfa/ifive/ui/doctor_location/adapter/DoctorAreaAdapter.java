package com.jrksfa.ifive.ui.doctor_location.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jrksfa.ifive.R;
import com.jrksfa.ifive.adapter.spinner_adapter.AddDoctorAreaAdapter;
import com.jrksfa.ifive.datas.SessionManager;
import com.jrksfa.ifive.datas.realm_models.AllTown;
import com.jrksfa.ifive.ui.add_doctor.fragment.AddDoctorFragment;
import com.jrksfa.ifive.ui.doctor_location.DoctorLocation;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DoctorAreaAdapter extends  RecyclerView.Adapter<DoctorAreaAdapter.MyViewHolder> {
    private Context context;
    private List<AllTown> cartList;
    private ArrayList<AllTown> townList;
    DoctorLocation doctorLocation;
    SessionManager sessionManager;
    Integer townID;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout viewForeground;
        public TextView townName, serialNumber;
        public MyViewHolder(View view) {

            super(view);
            townName = view.findViewById(R.id.town_name);
            serialNumber = view.findViewById(R.id.serial_number);
            viewForeground = view.findViewById(R.id.view_foreground);
        }
    }

    public DoctorAreaAdapter(Context context, List<AllTown> cartList, DoctorLocation doctorLocation, int townID) {
        this.context = context;
        this.cartList = cartList;
        this.doctorLocation = doctorLocation;
        this.townID = townID;
        this.townList = new ArrayList<>();
        this.townList.addAll(cartList);
    }

    @Override
    public DoctorAreaAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.town_list, parent, false);

        return new DoctorAreaAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DoctorAreaAdapter.MyViewHolder holder, final int position) {
        final AllTown item = cartList.get(position);
        holder.townName.setText(item.getTownName());
        holder.serialNumber.setText(position+1+"");
        holder.viewForeground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doctorLocation.setSaleFromTownPreference(item.getTownId(),item.getTownName());
            }
        });
        if((townID != 0) && (townID.equals(item.getTownId()))){
            holder.viewForeground.setBackground(context.getResources().getDrawable(R.drawable.blue_bg));
            holder.townName.setTextColor(context.getResources().getColor(R.color.white));
        }else{
            holder.viewForeground.setBackgroundColor(context.getResources().getColor(R.color.lite_grey3));
            holder.townName.setTextColor(context.getResources().getColor(R.color.black_low));
        }
    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        cartList.clear();
        if (charText.length() == 0) {
            cartList.addAll(townList);
        }else{
            for (AllTown wp : townList){
                if (wp.getTownName().toLowerCase(Locale.getDefault()).contains(charText)){
                    cartList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }


}
