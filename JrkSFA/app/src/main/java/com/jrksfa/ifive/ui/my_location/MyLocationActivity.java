package com.jrksfa.ifive.ui.my_location;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jrksfa.ifive.R;
import com.jrksfa.ifive.ui.base.BaseActivity;

public class MyLocationActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_location);
    }
}
