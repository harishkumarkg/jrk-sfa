package com.jrksfa.ifive.ui.stockist_dcr;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;

import com.jrksfa.ifive.R;
import com.jrksfa.ifive.ui.doctor_dcr.fragment.ViewDoctorDCRFragment;
import com.jrksfa.ifive.fragment.StockistAddDCR;
import com.jrksfa.ifive.ui.base.BaseActivity;

public class StockistDCRActivity extends BaseActivity {

    public static StockistDCRActivity instance;

    private StockistAddDCR fragmentOne;
    private ViewDoctorDCRFragment fragmentTwo;
    private TabLayout allTabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doctor_dcr);
        instance=this;

        allTabs = (TabLayout)findViewById(R.id.tabs);
        bindWidgetsWithAnEvent();
        setupTabLayout();
    }

    public static StockistDCRActivity getInstance() {
        return instance;
    }

    private void setupTabLayout() {
        fragmentOne = new StockistAddDCR();
        fragmentTwo = new ViewDoctorDCRFragment();

        allTabs.addTab(allTabs.newTab().setText("Add DCR"),true);
        allTabs.addTab(allTabs.newTab().setText("View DCR"));
    }

    private void bindWidgetsWithAnEvent()
    {
        allTabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void setCurrentTabFragment(int tabPosition)
    {
        switch (tabPosition)
        {
            case 0 :
                replaceFragment(fragmentOne);
                break;
            case 1 :
                replaceFragment(fragmentTwo);
                break;
        }
    }

    public void replaceFragment(android.support.v4.app.Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_container, fragment);
        ft.setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }
}
