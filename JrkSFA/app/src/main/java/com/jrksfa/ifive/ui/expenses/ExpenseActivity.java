package com.jrksfa.ifive.ui.expenses;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;

import com.jrksfa.ifive.R;
import com.jrksfa.ifive.ui.doctor_dcr.fragment.ViewDoctorDCRFragment;
import com.jrksfa.ifive.fragment.ExpenseAdd;
import com.jrksfa.ifive.ui.base.BaseActivity;

public class ExpenseActivity extends BaseActivity {

    public static ExpenseActivity instance;

    private ExpenseAdd fragmentOne;
    private ViewDoctorDCRFragment fragmentTwo;
    private TabLayout allTabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doctor_dcr);
        instance=this;

        allTabs = (TabLayout)findViewById(R.id.tabs);
        bindWidgetsWithAnEvent();
        setupTabLayout();
    }



    public static ExpenseActivity getInstance() {
        return instance;
    }




    private void setupTabLayout() {
        fragmentOne = new ExpenseAdd();
        fragmentTwo = new ViewDoctorDCRFragment();

        allTabs.addTab(allTabs.newTab().setText("Add EXP"),true);
        allTabs.addTab(allTabs.newTab().setText("View EXP"));
    }

    private void bindWidgetsWithAnEvent()
    {
        allTabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void setCurrentTabFragment(int tabPosition)
    {
        switch (tabPosition)
        {
            case 0 :
                replaceFragment(fragmentOne);
                break;
            case 1 :
                replaceFragment(fragmentTwo);
                break;
        }
    }

    public void replaceFragment(android.support.v4.app.Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_container, fragment);
        ft.setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }
}
